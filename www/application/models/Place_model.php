<?php
/*
 * Map_model
 */
class Place_model extends MY_Model {
	/**
	 * 지도에 등록된 장소의 목록을 가져옵니다.
	 */
	function getPlaces($args) {
		// 지도의 장소 정보 (추가정보 포함)
		$this->db->order_by('ISNULL(place.order), place.order asc, place.id asc')->from('place');
		if(isset($args['mapId']) && $args['mapId']) $this->db->where('place.mapId', $args['mapId']);
		if(isset($args['userId']) && $args['userId']) $this->db->where('place.userId', $args['userId']);
		if(!isset($args['isExport']) || $args['isExport'] != true) {
			$this->db->select('place.*, `index`.name as indexName, `index`.icon as indexIcon');
			$this->db->join('`index`', '`index`.`id` = `place`.`indexId`', 'left');
			$places = $this->db->get()->result();
			foreach($places as $place) {
				if($place->photos) {
					$photos = json_decode($place->photos);
					$place->photo = $this->_getPhotosThumbnail($photos[0]);
					unset($place->photos);
				}
			}
		} else if($args['isExport'] == true) {
			$this->db->select('place.name, place.address, place.description, place.path, `index`.name as indexName');
			$this->db->join('`index`', '`index`.`id` = `place`.`indexId`', 'left');
			$places = $this->db->get()->result();
		}
		
		return $places;
	}
	
	/**
     * 장소의 기본 정보를 가져옵니다.
     */
	function getPlace($placeId) {
		/*
			select place.*, users_oauth.services_name, users_oauth.services_portrait_s from place
			left join users_oauth on place.userId = users_oauth.users_id
		*/
		
		$this->db->select('place.*, users_oauth.services_name, users_oauth.services_portrait_s');
		$this->db->from('place');
		$this->db->join('users_oauth', 'place.userId = users_oauth.users_id', 'left');
		$this->db->where('place.id', $placeId);
		
		$place = $this->db->get()->row();
		
		if(trim($place->photos)) {
			$photos = json_decode($place->photos);
			$place->photos = array();
			foreach($photos as $photo) {
				array_push($place->photos, array(
					'filename' => $photo,
					'thumb_url' => $this->_getPhotosThumbnail($photo),
					'url' => $this->_getPhotosUrl($photo))
				);
			}
		} else unset($place->photos);
		return $place;
	}
	
	function getFlags($placeId = null) {
		if($placeId == null) {
			/*
			select place.name as placeName, map.name as mapName
			from place_flags
			left join place on place.id = place_flags.placeId
			left join map on place.mapId = map.id
			where place_flags.userId = 4
			*/
			$this->db->select('place.id, place.name as placeName, map.name as mapName, place_flags.createDate');
			$this->db->from('place_flags');
			$this->db->join('place', 'place.id = place_flags.placeId', 'left');
			$this->db->join('map', 'place.mapId = map.id', 'left');
			$this->db->where('place_flags.userId', $this->session->user_id);
			$this->db->where('place_flags.status', 0);
			$this->db->order_by('place_flags.id DESC');
			return $this->db->get()->result();	
		} else if($placeId != null) {
			return $this->db->get_where('place_flags', array('placeId' => $placeId, 'status' => '0'))->result();
		}
	}
	
	function editPlace($args, $method) {
		$data = array(
			'path'        => $args['path'],
			'indexId'     => $args['indexId'],
			'name'        => $args['name'],
			'address'     => $args['address'],
			'description' => htmlspecialchars($args['description']),
			'photos'      => ( isset($args['photos_filename']) && count($args['photos_filename']) ) ? json_encode($args['photos_filename']) : ''
		);
		
		if(isset($args['placeId'])) {
			$place = $this->getPlace($args['placeId']);
		}
		
		$this->load->library('ion_auth');
		
		if($method === 'create') {
			$data['mapId'] = $args['mapId'];
			$data['userId'] = $this->session->user_id;
			$this->db->insert('place', $data);
			$placeId = $this->db->insert_id();
			if(isset($args['field_id']) && isset($args['field_data'])) $this->_ff($args['field_id'], $args['field_data'], $placeId);
		} else if($method === 'modify' && ($place->userId === $this->session->user_id) || $this->ion_auth->is_admin()) {
			if($place->userId !== $this->session->user_id) { header("/"); }
			
			$this->db->set($data)->where('id', $args['placeId'])->update('place');
			$placeId = $args['placeId'];
			
			$this->_ff($args['field_id'], $args['field_data'], $placeId);
			$this->db->where('placeId', $placeId)->update('place_flags', array('status' => 1));
		} else if($method === 'modify' && ($place->userId !== $this->session->user_id)) {
			$data = array(
				'placeId' => $place->id,
				'userId' => $place->userId,
				'flagsBy' => $this->session->user_id,
				'description' => htmlspecialchars($args['description'])
			);
			$this->db->insert('place_flags', $data);
			$placeId = $this->db->insert_id();
		}
		
		return $placeId;
	}
	
	public function _ff($fieldIds, $fieldDatas, $placeId, $needJson = false) {
		if(isset($fieldIds) && count($fieldIds)) {
			if($needJson === true) $return = array();
			foreach($fieldIds as $i => $fieldId) {
				$data = array(
					'fieldId' => $fieldId,
					'placeId' => $placeId,
					'value' => $fieldDatas[$i]
				);
				if($needJson === true) array_push($return, $data);
				else {
					if ($this->db->get_where('field_data', array('placeId' => $placeId, 'fieldId' => $fieldId))->num_rows()) {
						$this->db->where(array('placeId' => $placeId, 'fieldId' => $fieldId));
						
						// 필드에 값이 있는 경우 업데이트, 그렇지 않은 경우 삭제
						if(trim($fieldDatas[$i])) {
							$this->db->update('field_data', $data);
						} else {
							$this->db->delete('field_data');
						}
					} else if(trim($fieldDatas[$i])) {
						$this->db->insert('field_data', $data); // 필드에 값이 있는 경우만 추가
					}	
				}
			}
			
			if($needJson === true) return json_encode($return);
		}
		
		
	}
	
	/**
	 * 장소의 추가 정보를 가져옵니다.
	 */
	function getPlaceFieldData($placeId) {
		$this->db->select('field.id, field.name, field_data.placeId, field_data.value');
		$this->db->from('field');
		$this->db->join('field_data', 'field_data.fieldId = field.id', 'left');
		$this->db->where('field_data.value !=', '');
		$this->db->where('field_data.placeId', $placeId);
		$this->db->order_by('field.id', 'asc');
		$this->db->order_by('field_data.placeId', 'asc');
		return $this->db->get()->result();
	}
	
	public function changePlaceOrder($places_idx) {
		$place_order = 1;
		
		if( isset($places_idx) && count($places_idx) ) {
			foreach($places_idx as $place_idx) {
				$this->db->where('id', $place_idx)->update('place', array('order' => $place_order++));
			}
		}
		
		return count($places_idx);
	}
	
	public function getComments($placeId) {
		return $this->db->get_where('comment', array('placeId' => $placeId));
	}
}