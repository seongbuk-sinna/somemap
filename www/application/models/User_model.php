<?php
/*
 * Map_model
 */
class User_model extends MY_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getMyData() {
		$maps = $this->db->order_by('map.id', 'desc')->get_where('map', array('map.userId' => $this->session->userdata('user_id')))->result();
		foreach($maps as $map) {
			if($map->thumbnail) $map->thumbnailUrl = $this->_getPhotosThumbnail($map->thumbnail);
		}
		
		$places = $this->db->order_by('place.id', 'desc')->get_where('place', array('place.userId' => $this->session->userdata('user_id')))->result();
		foreach($places as $place) {
			if($place->photos) {
				$photos = json_decode($place->photos);
				$place->photo = $this->_getPhotosUrl('thumbs/'.$photos[0]);
			} else {
				$place->photo = '';
			}
		}
		
		return array('maps' => $maps, 'places' => $places);
	}
	
	public function getUser($userId) {
		return $this->db->get_where('users_oauth', array('users_id' => $userId))->row();
	}
}