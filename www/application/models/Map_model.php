<?php
/*
 * Map_model
 */
class Map_model extends MY_Model {

	function getCategories($isNeedAll = true) {
		$query = $this->db->query(' SELECT category.id, category.permalink, category.name, map.thumbnail
									FROM (SELECT * FROM map WHERE thumbnail != \'\' ORDER BY rand()) map, category
									WHERE category.id = map.categoryId
									GROUP BY map.categoryId');
		
		$categories = $query->result();
		foreach($categories as $category) {
			if(trim($category->thumbnail)) {
				$category->thumbnailUrl = $this->_getPhotosThumbnail($category->thumbnail);
			}
		}
		return $categories;
	}
	
	/**
	 * 지도 목록을 가져옵니다.
	 */
	function getMaps($args) {
		if($args['category'] && $args['category'] != 'all') {
			$this->db->join('category', 'category.id = map.categoryId', 'left');
			$this->db->where('category.permalink', $args['category']);
			if($args['isOpen'] === 'Y') $this->db->where('map.isOpen', $args['isOpen']);
			$args['totalNumberOfMaps'] = $this->db->get('map')->num_rows();
			
			$this->db->join('category', 'category.id = map.categoryId', 'left');
			$this->db->where('category.permalink', $args['category']);
		} else {
			if(isset($args['isOpen']) && $args['isOpen'] === 'Y') $this->db->where('map.isOpen', $args['isOpen']);
			$args['totalNumberOfMaps'] = $this->db->get('map')->num_rows();
		}
		
		// 열린 지도만
		if(isset($args['isOpen']) && $args['isOpen'] === 'Y') {
			$this->db->where('map.isOpen', $args['isOpen']);
		}
		
		if($args['pageSize'] > 0) {// 페이징 설정
			$this->db->limit($args['pageSize'], (($args['currentNumberOfPage'] - 1) * $args['pageSize']));
			$args['totalNumberOfPages'] = ceil( $args['totalNumberOfMaps'] / $args['pageSize'] );
		}
		
		$this->db->select('map.id, map.name, map.thumbnail, map.description, IFNULL(places.placecount, 0) as placecount, users_oauth.services_name, users_oauth.services_portrait_s');
		$this->db->from('map');
		$this->db->join('users_oauth', 'map.userId = users_oauth.users_id', 'left');
		$this->db->join('(select place.mapId, count(place.id) as placecount from place group by place.mapId) places', 'map.id = places.mapId', 'left');
		if(isset($args['whereIn'])) $this->db->where_in('map.id', $args['whereIn']);
		if(isset($args['userId'])) $this->db->where('map.userId', $args['userId']);
		if(isset($args['keyword']) && $args['keyword']) {
			$this->db->where('map.name like "%'.$args['keyword'].'%"');
			$this->db->or_where('map.description like "%'.$args['keyword'].'%"');
		}
		
		if(isset($args['featured']) && $args['featured'] == true ) {
			$this->db->where('map.featured >=', 1); 
			$this->db->order_by('map.featured', 'desc'); // 피쳐드 설정된 지도 먼저
		}
		
		// 지도 정렬 기준
		if($args['orderType'] == "recent") {
			$this->db->order_by('map.id', 'desc'); // 최신순
		} else {
			$this->db->order_by('map.hit', 'desc'); // 조회순
		}
		
    // 긁기
		$maps = $this->db->get();
		
		// 1개 이상 존재하면 CF URL 변환하고 리턴
		if($maps->num_rows() !== 0) {
			$maps = $maps->result();
			
			// API용 필드 데이터 변환
			foreach($maps as $map) {
				if(trim($map->thumbnail)) {
					$map->thumbnailUrl = $this->_getPhotosThumbnail($map->thumbnail);
				}
			}
			
			return array('maps' => $maps, 'conditions' => $args);	
		// 1개도 없으면 NULL 리턴
		} else {
			return null;
		}
	}
	
	/**
	 * 지도 1개의 기본 정보를 가져옵니다.
	 */
	function getMap($mapId) {
		$this->db->select('map.id, map.categoryId, map.name, map.description, map.thumbnail, map.hit, map.userId, map.permission, map.password, map.featured, IFNULL(places.placecount, 0) as placecount, users_oauth.services_name, users_oauth.services_portrait_s');
		$this->db->from('map');
		$this->db->join('users_oauth', 'map.userId = users_oauth.users_id', 'left');
		$this->db->join('(select place.mapId, count(place.id) as placecount from place group by place.mapId) places', 'map.id = places.mapId', 'left');
		$this->db->where("map.id", $mapId);
		$map = $this->db->get()->row();
		if($map && $map->thumbnail) {
			$map->thumbnailUrl = $this->_getPhotosUrl($map->thumbnail);
		}
		return $map;
	}
	
	function editMap($args, $mapId) {
		// 지도 등록
		$data = array(
			'name'    => $args['map_name'],
			'description' => htmlspecialchars($args['map_description']),
			'categoryId' => isset( $args['map_category'] ) ? $args['map_category'] : 20,
			'permission' => $args['map_permission'],
			'password' => ($args['map_permission'] == 2 ? $args['map_password'] : '')
		);
		
		if(!$mapId) {
			$data['userId'] = $this->session->user_id;
			$this->db->insert('map', $data);
			$mapId = $this->db->insert_id();
		} else {
			$this->db->where('id', $mapId)->update('map', $data);	
		}
		
		// 새로 추가된 사진 정보 업데이트
		if( isset($args['photos_filename']) && count($args['photos_filename']) ) {
			$this->db->where('id', $mapId)->update('map', array('thumbnail' => $args['photos_filename'][0]));
		} else {
			$this->db->where('id', $mapId)->update('map', array('thumbnail' => ''));
		}
		
		// 필드 등록
		if( isset($args['field_id']) && count($args['field_id']) ) {
			$field_order = 1;
			foreach($args['field_id'] as $key => $fieldId) {
				if($fieldId === 'new') {
					$this->db->insert('field', array(
							'mapId' => $mapId, 
							'name' => $args['field_name'][$key],
							'order' => $field_order++
						)	
					);
				} else if($fieldId) {
					$this->db->where('id', $fieldId)->update('field', array(
							'name' => $args['field_name'][$key],
							'order' => $field_order++
						)	
					);
				}
			}
		}
		
		if( isset($args['index_id']) && count($args['index_id']) ) {
			foreach($args['index_id'] as $key => $indexId) {
				// 새로 등록된 하위구분인 경우 등록
				if($indexId === 'new') {
					$this->db->insert('index', array(
							'mapId'   => $mapId,
						    'name' => $args['index_name'][$key],
						    'icon' => $args['index_image'][$key]
						)
					);
				} else if($indexId) {
					$this->db->where('id', $indexId)->update('index', array(
							'name' => $args['index_name'][$key],
						    'icon' => $args['index_image'][$key]
						)
					);
				}
			}
		}
		
		return $mapId;
	}
	
	/**
	 * 지도에 등록된 범례의 목록을 가져옵니다.
	 */
	function getIndexes($mapId) {
		$this->db->select('index.id, index.name, index.icon');
		$this->db->order_by('index.id', 'asc')->from('index')->where('index.mapId', $mapId);
		$indexes = $this->db->get()->result();

		// API용 필드 데이터 변환 (장소구분)
		foreach($indexes as $index) {
			$index->id = (int) $index->id;
			$index->iconUrl = "/assets/img/".$index->icon; // $this->_getPhotosUrl($index->icon)
		}
		
		return $indexes;
	}
	
	function getFields($mapId) {
		return $this->db->order_by('field.order', 'asc')->get_where('field', array('field.mapId' => $mapId))->result();
	}
	
	/**
	 * 사진 1개의 정보를 등록합니다.
	 */
	function insertPhoto($args) {
		// DB에 입력
		return array('filename' => $args['photo_path'], 'url' => $this->_getPhotosUrl($args['photo_path']));
	}

	/**
	 * Get simple map data by map id
	 * @param $id map id
	 */
	function getMapById($id) {
		$query = $this->db->get_where('map', array('id' => $id, 'is_open' => 'Y'));

		return $query->row();
	}

	/**
	 * 엑셀 데이터 추출용 조회 데이터
	 * @param $map_id
	 * @return array
	 */
	function getMapDataForExcelExport($map_id) {
		// read map_data
		$this->db->select('place.id,
				place.map_id,
				place.latitude 위도,
				place.longitude 경도,
				map_pin.pin_name 장소구분,
				place.map_data_name 이름,
				place.address 주소,
				place.comment 코멘트');
		$this->db->from("map_data");
		$this->db->join('map_pin', 'map_pin.id = place.map_pin_id', 'left');
		$this->db->where("place.map_id", $map_id);

		$map_data = $this->db->get();

		// attach field data
		$map_data_array = $map_data->result_array ();
		foreach ( $map_data_array as &$map_data_row ) {

			// 커스텀 필드
			$this->db->select('map_field.field_name,
							   map_field_data.value');
			$this->db->from('map_field');
			$this->db->join('map_field_data', 'map_field_data.map_field_id = map_field.id AND map_field_data.map_data_id = '.$map_data_row["id"], 'left');
			$this->db->where('map_field.map_id', $map_data_row["map_id"]);

			$map_field = $this->db->get();

			foreach ( $map_field->result_array () as $map_field_row ) {
				$map_data_row[$map_field_row["field_name"]] = $map_field_row["value"];
			}
		}
		return $map_data_array;
	}
}