		<div class="l--board">
			<div class="l--container">
				<iframe style="width: 100%;" frameborder="0" src="/board/board/notice"></iframe>
			</div>
		</div>
		
		<?php include_once(APPPATH.'views/templates/footer.php'); ?>
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
		<script src="/assets/jquery.browser.js"></script>
		<script src="/assets/jquery-iframe-auto-height.min.js"></script>
		<script>
			$('iframe').iframeAutoHeight({
				minHeight: 240, // Sets the iframe height to this value if the calculated value is less
				heightOffset: 10 // Optionally add some buffer to the bottom
			});

		</script>
	</body>
</html>