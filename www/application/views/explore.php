		<div class="l--main--filters">
			<ul>
				<li>
					<a href="/explore/?o=<?php echo ((isset($_GET['o']) and $_GET['o'] === "recent") or !isset($_GET['o'])) ? "popular" : "recent" ?>" 
						class="<?php echo !isset( $_GET['c'] ) ? "selected" : "" ?> <?php echo ((isset($_GET['o']) and $_GET['o'] === "recent") or !isset($_GET['o'])) ? "popular" : "recent" ?>">
						전체
						<?php if( !isset( $_GET['c'] ) ): ?>
						<span class="triangle"></span>
						<?php endif; ?>
					</a>
				<?php foreach($categories as $category) { ?>
				<?php 
					if ( (isset($_GET['o']) and $_GET['o'] === "recent") or !isset($_GET['o']) )  { 
						$order = "popular";
					} else if ( isset($_GET['o']) and $_GET['o'] === "popular" ) { 
						$order = "recent"; 
					}
				?>
				<li>
					<a href="/explore/?c=<?php echo $category->permalink ?><?php echo ((isset($_GET['c']) and ($_GET['c']==$category->permalink)) ? "&o=".$order : '') ?>" 
					   class="<?php echo (isset($_GET['c'])&&$_GET['c']==$category->permalink)?"selected":""?> <?php echo $order ?>">
						<?=$category->name?>
						<?php if( isset( $_GET['c'] ) && $_GET['c'] == $category->permalink ): ?>
						<span class="triangle"></span>
						<?php endif; ?>
					</a>
				</li>
				<?php } ?>					
			</ul>
		</div>
		
		<div class="l--main">
			<div class="l--container">
				<div class="c--cards">
				<?php $colorset = array('#914626', '#F1409F', '#AADE3A', '#2D5CE6', '#5AC1F5', '#21A93C', '#E0B73F', '#EC413B'); ?>
				<?php foreach($maps as $m) { ?>
				<h1 class="c--cards-title"><?=isset($m['conditions']['categoryName'])?$m['conditions']['categoryName']:"전체"?></h1>
				<?php if( isset($m['maps']) ) { ?>
				<?php foreach($m['maps'] as $map) { ?>
					<a class="c--card" href="/map/<?=$map->id?>">
						<?php if($map->thumbnail) { ?>
						<div class="c--card-hero" style="background-image:url('<?=$map->thumbnailUrl?>')">
							<span class="c--card-category"></span>
						</div>
						<div class="c--card-meta">
							<h1 class="c--card-title"><?=$map->name?></h1>
						<?php } else { ?>
						<div class="c--card-hero" style="background: <?=$colorset[array_rand($colorset)]?>;">
							<span class="c--card-title"><?=$map->name?></span>
						</div>
						<div class="c--card-meta">
						<?php } ?>
							<h3 class="c--card-creator">
									<span class="s--portrait" <?=$map->services_portrait_s?'style="background-image:url(\''.$map->services_portrait_s.'\')"':''?>></span> <?=$map->services_name?> / <?=$map->placecount?>개의 장소
								</h3>
								<p class="c--card-desc"><?=$map->description?></p>
						</div>
						<div class="c--card-tail"></div>
					</a>
					<?php }}} ?>
				</div>
			</div>
		</div>
		
		<?php include_once(APPPATH.'views/templates/footer.php'); ?>
	</body>
</html>