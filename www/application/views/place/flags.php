		<div class="l--common-page-header">
			<h1>수정 제안</h1>
		</div>
		
		<div class="l--edit">
			<form method="POST">
				<label class="l--edit--item-label" for="description">어느 부분이 틀렸나요?</label>
				<div class="l--edit--block">
					<textarea class="form-control" id="description" placeholder="설명" name="description" rows="7"><?=isset($place)?$place->description:""?></textarea>
				</div>
				
				<input type="submit" class="s--button d--block f--do-submit" value="등록"></div>
			</form>
		</div>
		
		<?php 
			if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net")
				include_once(APPPATH.'views/songnae/templates/footer.php');
			else include_once(APPPATH.'views/templates/footer.php');
		?>
	</body>
</html>