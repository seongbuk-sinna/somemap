		<div class="l--common-page-header">
			<h1><?=isset($place)?"장소 수정하기":"장소 추가하기"?> </h1>
		</div>
		
		<div class="l--edit" id="l--edit">
			<input type="checkbox" name="visible-required-item" id="visible-required-item" class="f--dropdown-checkbox" <?=isset($place)?"":"disabled"?> />
			<label for="visible-required-item" class="l--edit--items-block--title">
				위치 지정
				<div class="inverted-triangle">
					<span class="triangle-one"></span>
					<span class="triangle-two"></span>
				</div>
			</label>
			<div class="l--edit--items-block l--edit--must-item">
				<input type="text" id="title" placeholder="추가할 곳의 이름이나 주소를 입력하세요." name="title" value="<?=isset($place)?$place->name:""?>" required class="search-input" />
				<ul id="l--places-search" class="l--places list">
					<?php if(isset($place)) { ?>
					<li class="l--places--place" data-name="<?=$place->name?>" data-address="<?=$place->address?>"  data-path="<?=htmlspecialchars($place->path, ENT_QUOTES)?>">
						<a class="l--places--place--wrapper" href="#" style="background-image:url('/assets/img/pin-purple.png');">
							<span class="l--places--place-index"></span>
							<span class="l--places--place-name"><?=$place->name?></span>
							<span class="l--places--place-address"><?=$place->address?></span>
						</a>
						<div id="c--map">
							<div id="c--map-controller--change-state">
								<input type="radio" name="type" value="0" class="c--map-controller--draw-state" id="c--map-controller--draw-state-0" checked />
								<label for="c--map-controller--draw-state-0">보기</label>
			
								<input type="radio" name="type" value="1" class="c--map-controller--draw-state" id="c--map-controller--draw-state-1" />
								<label for="c--map-controller--draw-state-1">편집-점</label>
								<input type="radio" name="type" value="2" class="c--map-controller--draw-state" id="c--map-controller--draw-state-2" />
								<label for="c--map-controller--draw-state-2">편집-선/면</label>
							</div>
							<div class="c--map-controller--current-location" id="map-current-location">현재위치</div>
							<div class="c--map-canvas" id="edit-canvas"></div>
							<label for="visible-required-item" class="c--map-canvas--use-this-place f--dropdown-switch" id="confirm-this-location">이 장소 사용하기</label>
						</div>
					</li>
					<?php } else { ?>
					<li>
						<div id="c--map">
							<div id="c--map-controller--change-state">
								<input type="radio" name="type" value="0" class="c--map-controller--draw-state" id="c--map-controller--draw-state-0" checked />
								<label for="c--map-controller--draw-state-0">보기</label>
			
								<input type="radio" name="type" value="1" class="c--map-controller--draw-state" id="c--map-controller--draw-state-1" />
								<label for="c--map-controller--draw-state-1">편집-점</label>
								<input type="radio" name="type" value="2" class="c--map-controller--draw-state" id="c--map-controller--draw-state-2" />
								<label for="c--map-controller--draw-state-2">편집-선/면</label>
							</div>
							<div class="c--map-controller--current-location" id="map-current-location">현재위치</div>
							<div class="c--map-canvas" id="edit-canvas"></div>
							<label for="visible-required-item" class="c--map-canvas--use-this-place f--dropdown-switch" id="confirm-this-location">이 장소 사용하기</label>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
			
			
			<form method="POST">
				<label for="visible-required-item" class="l--edit--items-block--title">
					기본 정보
					<div class="inverted-triangle">
						<span class="triangle-one"></span>
						<span class="triangle-two"></span>
					</div>
				</label>
				<div class="l--edit--items-block l--edit--required-item">
					<div class="l--edit--item-block">
						<label for="name" class="l--edit--item-label">이름</label>
						<input type="text" id="name" placeholder="이름" name="name" value="<?=isset($place)?$place->name:""?>" required="required" autocomplete="off" />
					</div>
					<div class="l--edit--item-block">
						<label for="address" class="l--edit--item-label">주소</label>
						<input type="text" id="address" class="address" placeholder="주소" name="address" value="<?=isset($place)?$place->address:""?>" autocomplete="off" />
						<input type="hidden" name="path" id="path" placeholder="path" value="<?=isset($place->path)?htmlspecialchars($place->path, ENT_QUOTES):""?>" />
					</div>
					
					<div class="l--edit--item-block">
						<label for="description" class="l--edit--item-label">설명</label>
						<textarea class="form-control" id="description" placeholder="설명" name="description"><?=isset($place)?$place->description:""?></textarea>
					</div>
					
					<div class="l--edit--item-block">
						<?php if( isset($indexes) && count($indexes) ) { ?>	
						<label for="indexId" class="l--edit--item-label">카테고리</label>
						<div class="l--edit--block--radio-wrapper">
							<?php foreach($indexes as $index) { ?>
							<input type="radio" name="indexId" id="indexId_<?=$index->id?>" value="<?=$index->id?>" <?=isset($place)&&$place->indexId==$index->id?"checked":""?> />
							<label for="indexId_<?=$index->id?>"><?=$index->name?></label>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
					
					<?php if($map->permission == 2) { ?>
					<div class="l--edit--item-block">
						<label for="map_password" class="l--edit--item-label">지도 편집 비밀번호</label>
						<input type="password" id="map_password" placeholder="사전에 안내받은 비밀번호를 입력해주세요." name="map_password" autocomplete="off" required />
					</div>
					<?php } ?>
				</div>
				
				<label for="visible-optional-item" class="l--edit--items-block--title">
					추가 정보
					<div class="inverted-triangle">
						<span class="triangle-one"></span>
						<span class="triangle-two"></span>
					</div>
				</label>
				<input type="checkbox" name="visible-optional-item" id="visible-optional-item" class="f--dropdown-checkbox" checked />
				<div class="l--edit--items-block l--edit--optional-item">
					<?php 
					if($fields) {
						foreach($fields as $field) {
							echo '<div class="l--edit--item-block">';
							echo '<label class="l--edit--item-label">'.$field->name.'</label>';
							echo '<input name="field_id[]" value="'.$field->id.'" type="hidden" />';
							echo '<textarea placeholder="'.$field->name.'" name="field_data[]">';
	                            if (isset($fields_data)) {
	                            	foreach( $fields_data as $field_data ) {
	                                	if( $field_data->id === $field->id ) echo $field_data->value;
	                                }
	                            }
	                        echo '</textarea>';
	                        echo '</div>';
	                    }
					}
					?>
					
					<div class="l--edit--item-block">
						<label class="l--edit--item-label">미디어 첨부</label>
						<div class="l--edit--block--attachement-double">
							<input type="file" multiple accept="image/*" id="fileupload" name="photo" />
						</div>
						
						<div class="l--edit--uploads-list">
						<?php
							if( isset($place->photos) && $place->photos ) {
								foreach($place->photos as $photo) {
									echo '<div class="l--edit--uploads-photo">';
									echo '  <div class="photo" style="background: url(\''.$photo['thumb_url'].'\') center / cover no-repeat;"></div>';
									echo '  <input name="photos_filename[]" type="hidden" value="'. $photo['filename'] .'" />';
									echo '  <div class="overlay">삭제?</div>';
									echo '</div>';
								}
							}
						?>
						</div>
					</div>
				</div>
				
				<div class="l--edit--items-block">
					<button class="s--button d--block f--do-submit">등록</button>
				</div>
			</form>
		</div>
		
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
		<script type="x-tmpl-mustache" id="template--search-result">
			<li class="l--places--place" data-name="{{dataTitle}}" data-address="{{dataAddress}}" data-path="{{dataPath}}">
				<a class="l--places--place--wrapper" href="#" style="background-image:url('/assets/img/pin-purple.png');">
					<span class="l--places--place-index">{{category}}</span>
					<span class="l--places--place-name">{{title}}</span>
					<span class="l--places--place-address">{{address}}</span>
				</a>
			</li>	
		</script>
		<script>
			var ev = new Somemap();
			ev.initCanvas('edit-canvas', {
				'center': new daum.maps.LatLng(37.566677, 126.978414), 
				'level': 3
			});
			ev.initKeywordSearch();
			ev.initListenersForDataEdit();
			<?php if(isset($place)) { ?> $("#l--places-search").find('a').click(); <?php } ?>
			
			$('#c--map-controller--change-state').on('change', '.c--map-controller--draw-state', function(e){ev.stateChange(e.target.value*1);});
			$(ev.map.getNode()).on(("ontouchstart" in window ? "touchend" : 'click'), 'span.path-dot', {'this': ev}, ev.processDots);
			$("#visible-required-item").on('change', function(){ev.map.relayout();});
			$("form label[for=visible-required-item]").click(function(e){ if(e.target.control.disabled===true)alert('먼저 위치를 지정해야 합니다.'); });
		</script>
	</body>
</html>