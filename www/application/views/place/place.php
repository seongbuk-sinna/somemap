		<div class="l--place">	    	
	    	<div class="c--card">
				<?php if(isset($place->photos)) { ?>
				<?php if(count($place->photos) > 1) { ?>
				<div class="c--card-hero">	
					<div class="owl-carousel">
					    <?php foreach($place->photos as $photo) { ?>
					    <div class="item" style="background-image:url('<?=$photo['thumb_url']?>');">
						    <a href="<?=$photo['url']?>" target="_blank"></a>
					    </div>
					    <?php } ?>
					</div>
				<?php } else { ?>
				<div class="c--card-hero" style="background-image:url('<?=$place->photos[0]['url']?>')">
				<?php }} else { ?>
				<div class="map-canvas" id="place-canvas-<?=$place->id?>">
				<?php } ?>
					<div class="c--card-hero--nb">
						<button id="f--close-latest-place" class="c--card-hero--nb-element f--close-place-page" style="background:url('/assets/img/close.png') no-repeat center / 20px;"></button>
					</div>
				</div>
				<div class="c--card-meta">
					<p class="c--card-address"><?=$place->address?></p>
					<h1 class="c--card-title"><?=$place->name?></h1>
					<h3 class="c--card-creator">
						<span class="s--portrait" <?=$place->services_portrait_s?'style="background-image:url(\''.$place->services_portrait_s.'\')"':''?>></span> <?=$place->services_name?>
					</h3>
					<?php if(isset($place->description) && $place->description != '') { ?>
					<p class="c--card-desc"><?=nl2br($place->description)?></p>
					<?php } ?>
				</div>
				
				<?php if((isset($session['user_id']) && $place->userId === $session['user_id']) || $isAdmin === true) { ?>
				<div class="c--card-actions">
					<div class="c--card-actions--wrapper">
						<a href="/place/<?=$place->id?>/edit" class="c--card-actions--action">
							<div class="c--card-actions--action-glyphicon-wrapper">
								<span class="glyphicons glyphicons-edit"></span>
							</div>
							<div class="c--card-actions--action-name">장소 수정</div>
						</a>
						<a href="/place/<?=$place->id?>/remove" data-placeid="<?=$place->id?>" id="f--remove-place-trigger" class="c--card-actions--action">
							<div class="c--card-actions--action-glyphicon-wrapper">
								<span class="glyphicons glyphicons-floppy-remove"></span>
							</div>
							<div class="c--card-actions--action-name">장소 삭제</div>
						</a>
					</div>
				</div>
				<?php } ?>
				<div class="c--card-data" data-placeid=<?=$place->id?> data-path=<?=$place->path?>>
		    		<?php
						if(count($place->fieldData) > 0) {
							foreach($place->fieldData as $fieldData) {
					?>
					<h5 class="c--card-data-title"><?=$fieldData->name?></h5>
					<p class="c--card-data-value"><?=nl2br($fieldData->value)?></p>
					<?php }} ?>
		    	</div>
			</div>
			
			<?php if(isset($place->photos)) { ?> 
			<div class="map-canvas" id="place-canvas-<?=$place->id?>"></div>
			<?php } ?>
    	</div>