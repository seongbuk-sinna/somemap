        <?php 
			if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net")
				include_once(APPPATH.'views/songnae/templates/footer.php');
			else include_once(APPPATH.'views/templates/footer.php');
		?>
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
        <script>
			if(location.hash === "#_=_") location.hash = '';
			else if(location.hash) location.href = "/place/" + location.hash.substr(1);
			
			var pv = new Somemap();
			pv.loadPlaceData(location.pathname.substr(7));
		</script>
	</body>
</html>