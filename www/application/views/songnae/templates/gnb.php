	<body class="<?=isset($classes)?$classes:""?>">
		<div class="l--gnb">
	        <ul class="l--list l--list-hor l--gnb--menu float-left">
				<li class="l--list-element l--list-hor-element"><a href="/" class="logo">송내동</a></li>
		    </ul>
		    
		    <ul class="l--list l--list-hor l--gnb--menu float-right">
				<?php if(isset($session['user_id'])) { ?>
				<li class="l--list-element l--list-hor-element f--tooltip-trigger">
					<a>메뉴</a>
					<div class="l--tooltip">
						<a href="/map/edit">지도 만들기</a>
						<div class="l--list-divider horizontal"></div>
						<?php foreach($menus as $menu) { ?>
						    <a href="<?php echo $menu->me_link; ?>" target="_<?php echo $menu->me_target; ?>" ><?php echo $menu->me_name; ?></a>
						<? } ?>
					</div>
				</li>
				<?php } else { ?>
				<li class="l--list-element l--list-hor-element f--tooltip-trigger">
					<a>로그인</a>
					<div class="l--tooltip">
						<a class="btn kakao" href="/board/plugin/sns_plugin/login.php?type=kakao">카카오톡으로 로그인</a>
						<a class="btn facebook" href="/board/plugin/sns_plugin/login.php?type=facebook">페이스북으로 로그인</a>
						<a class="btn naver" href="/board/plugin/sns_plugin/login.php?type=naver">네이버로 로그인</a>
						<div class="l--list-divider horizontal"></div>
						<?php foreach($menus as $menu) { ?>
						    <a href="<?php echo $menu->me_link; ?>" target="_<?php echo $menu->me_target; ?>" ><?php echo $menu->me_name; ?></a>
						<? } ?>
					</div>
				</li>
				<?php } ?>
	        </ul>
		</div>