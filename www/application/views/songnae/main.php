		<div class="l--cards">
			<div class="l--cards--wrapper l--container">
			<?php foreach($data['maps'] as $map) { ?>
				<div class="l--cards--card">
					<a href="/map/<?=$map->id?>">
						<span class="l--cards--card-thumbnail" style="<?=$map->thumbnail?"background-image:url('".$map->thumbnail."')":""?>"></span>
						<span class="l--cards--card-title"><?=$map->name?></span>
						<span class="l--cards--card-description"><?=$map->description?></span>
						<span class="l--cards--card-counter"><?=$map->placecount?>개 장소 등록됨</span>
					</a>
				</div>
			<?php } ?>
			</div>
		</div>
		
		<?php include_once(APPPATH.'views/songnae/templates/footer.php'); ?>
	</body>
</html>