	    <input type="checkbox" name="toggle-switch-map-list" id="toggle-switch-map-list" class="f--dropdown-checkbox" />
	    <div class="l--map">
        	<div class="l--map--wrapper" id="l--map--wrapper">
	        	<div class="c--card">
					<?php if($map->thumbnail) { ?>
					<div class="c--card-hero" style="background-image:url('<?=$map->thumbnailUrl?>')">
					<?php } else { ?>
					<?php $colorset = array('#914626', '#F1409F', '#AADE3A', '#2D5CE6', '#5AC1F5', '#21A93C', '#E0B73F', '#EC413B'); ?>
					<div class="c--card-hero"  style="background: <?=$colorset[array_rand($colorset)]?>;">
						<span class="c--card-title"><?=$map->name?></span>
					<?php } ?>
						<div class="c--card-hero--nb">
							<a class="c--card-hero--nb-element f--close-map-page" href="/" style="background:url('/assets/img/close.png') no-repeat center / 20px;"></a>
							<label class="c--card-hero--nb-element f--dropdown-switch triple-circle white" for="toggle-expand-menu-in-map">
				    			<span></span>
					    		<span></span>
					    		<span></span>
					    	</label>
				    		<input type="checkbox" name="toggle-expand-menu-in-map" id="toggle-expand-menu-in-map" class="f--dropdown-checkbox" />
				    		<ul class="l--list">
					    		<?php if($isAdmin === true) { ?>
								<li class="l--list-element">
									<?php if($map->featured == 0) { ?>
									<a href="/map/<?=$map->id?>/featured/enable">피쳐드 지도로 설정하기</a>
									<?php } else if($map->featured != 0) { ?>
									<a href="/map/<?=$map->id?>/featured/disable">피쳐드 지도에서 해제하기</a>
									<?php } ?>
								</li>
								<?php } ?>
								<li class="l--list-element">
									<a href="/map/exportExcel/<?=$map->id?>">데이터 내보내기</a>
								</li>
								<li class="l--list-element">
									<a href="#" onclick='javascript:window.prompt("아래 내용을 복사하세요.", "<iframe src=\"http://somemap.kr/map/<?=$map->id?>\" width=\"400\" height=\"300\"></iframe>");'>임베드</a>
								</li>
								<!--li class="l--list-element">
									<a href="#">이미지 다운로드(준비중)</a>
								</li-->
							</ul>
							
						</div>
						<div class="c--card-hero--nb c--card-hero--nbb">
							<a class="twitter" href="http://twitter.com/intent/tweet?text=<?=htmlentities($map->name)?>&amp;url=http%3A%2F%2Fsomemap.kr%2Fmap%2F<?=$map->id?>&amp;hashtags=썸맵" target="_blank"></a>
							<a class="facebook" href="http://facebook.com/sharer.php?u=http://somemap.kr/map/<?=$map->id?>" target="_blank"></a>
							<a class="kakao hiding-over-540px--ib" href="#" class="f--to-kakao"></a>
						</div>
					</div>
					<div class="c--card-meta">
						<?php if($map->thumbnail) { ?>
						<h1 class="c--card-title"><?=$map->name?></h1>
						<?php } ?>
						<h3 class="c--card-creator">
							<span class="s--portrait" <?=$map->services_portrait_s?'style="background-image:url(\''.$map->services_portrait_s.'\')"':''?>></span>
							<a href="/users/<?=$map->userId?>/map"><?=$map->services_name?></a> / <?=$map->placecount?>개의 장소
						</h3>
						<p class="c--card-desc"><?=nl2br($map->description)?></p>
					</div>
					<div class="c--card-actions">
						<div class="c--card-actions--wrapper">
							<?php if($map->permission == 1 || $map->permission == 2 || ($map->permission == 3 && (isset($session['user_id']) && $map->userId == $session['user_id']))) { ?>
							<a href="/map/<?=$map->id?>/add-place" class="c--card-actions--action">
								<div class="c--card-actions--action-glyphicon-wrapper">
									<span class="glyphicons glyphicons-pen"></span>
								</div>
								<div class="c--card-actions--action-name">장소 추가</div>
							</a>
							<?php } ?>
							<?php if((isset($session['user_id']) && $map->userId === $session['user_id']) || $isAdmin === true) { ?>
							<a href="/map/<?=$map->id?>/edit" class="c--card-actions--action">
								<div class="c--card-actions--action-glyphicon-wrapper">
									<span class="glyphicons glyphicons-edit"></span>
								</div>
								<div class="c--card-actions--action-name">지도 수정</div>
							</a>
							<?php } ?>
							<?php if(count($places) === 0 && $map->userId === $session['user_id'] || $isAdmin === true) { ?>
							<a href="/map/<?=$map->id?>/remove" class="c--card-actions--action">
								<div class="c--card-actions--action-glyphicon-wrapper">
									<span class="glyphicons glyphicons-floppy-remove"></span>
								</div>
								<div class="c--card-actions--action-name">지도 삭제</div>
							</a>
							<?php } ?>
							<label class="c--card-actions--action f--switch-map-list hiding-over-540px--ib" for="toggle-switch-map-list">
								<div class="c--card-actions--action-glyphicon-wrapper">
									<span class="glyphicons glyphicons-cadastral-map"></span>
								</div>
								<div class="c--card-actions--action-name">지도 보기</div>
							</label>
						</div>
					</div>
				</div>
	        	
				<div id="l--index" class="l--index">
			        <ul class="l--indexes--wrapper">
				        <li class="l--indexes--index">
							<label style="background-image:url('/assets/img/pin-black.png');">
								<input type="checkbox" class="l--indexes--index-checkbox" checked="checked" value="all" />
								<i class="check"></i>
								전체
							</label>
						</li>
						<?php foreach($indexes as $index) { ?>
						<li class="l--indexes--index">
							<label style="<?=isset($index->iconUrl)?"background-image:url('".$index->iconUrl."')":""?>">
								<input type="checkbox" class="l--indexes--index-checkbox" checked="checked" value="<?=$index->id?>" data-iconurl="<?=$index->iconUrl?>" />
								<i class="check"></i>
								<?=$index->name?>
							</label>
						</li>	
		 				<?php } ?>
			        </ul>
		        </div>
				
				<div class="l--sort--search">
					<input type="search" class="search" />
				</div>
				
				<div class="l--sort--order">
					<button class="sort desc" data-sort="l--places--place-name">이름 순 정렬</button>
					<button class="sort" data-sort="l--places--place-id">생성 순 정렬</button>
					<button class="sort" data-sort="l--places--place-index">분류 별 정렬</button>
				</div>
				
				<ul id="l--places" class="l--places list">
					<?php foreach($places as $place) { ?>
					<li class="l--places--place" id="l--place-<?=$place->id?>" data-name="<?=$place->name?>" data-address="<?=$place->address?>" data-placeid="<?=$place->id?>" data-indexid="<?=$place->indexId?>" data-path="<?=htmlspecialchars($place->path, ENT_QUOTES)?>" data-iconurl="<?=$place->indexIcon?>">
						<a class="l--places--place--wrapper<?=isset($place->photo)?' p--thumbnail-yes':''?>" href="/place/<?=$place->id?>" style="background-image:url('/assets/img/<?=$place->indexIcon!=NULL?$place->indexIcon:'pin-black.png'?>');">
							<?php if(isset($place->photo)) { ?>
							<div class="l--places--place-thumbnail" style="background-image:url('<?=$place->photo?>')"></div>
							<?php } ?>
							<span class="l--places--place-index"><?=$place->indexName?></span>
							<span class="l--places--place-name"><?=$place->name?></span>
							<span class="l--places--place-address"><?=$place->address?></span>
							<span class="l--places--place-id"><?=$place->id?></span>
						</a>
					</li>
					<?php } ?>
				</ul>
        	</div>
        </div>
        
        <div class="l--canvas--wrapper">
	        <div class="map-canvas" id="map-canvas-<?=$map->id?>"></div>
	        <div class="map-current-location" id="map-current-location"></div>
	        <label for="toggle-switch-map-list" class="return-list"></label>
        </div>
        
        <?php include_once(APPPATH.'views/templates/scripts.php'); ?>
        <script>
	        if(location.hash === "#_=_") location.hash = '';
          else if(location.hash) location.href = "/map/" + location.hash.substr(1); 
			
    			// 지도 초기화
    			var mv = new Somemap();
    				
    			mv.initCanvas('map-canvas-' + location.pathname.substr(5), {'center': new daum.maps.LatLng(37.551681, 126.991775), 'level': 7})
    			  .addMarkersWithPlaces()
    			  .initListenersForMapView();
    			  
    			
    			Kakao.init('<?=gethostname() == 'vagrant-ubuntu-trusty-64'?'868c011168cdacea56434fdb46c05738':'d13cf4b3a27a93ec71ea5d9688e39978'?>');
    			Kakao.Link.createTalkLinkButton({
    				container: '.kakao.hiding-over-540px--ib',
    				label: '<?=addslashes($map->name)?>',
    				image: {src: '<?=$map->thumbnail?$map->thumbnailUrl:base_url('/assets/img/front-bg.jpg')?>',width: '300',height: '200'},
    				webButton:{text: '썸맵',url: '<?=current_url()?>'}
    			});
    			
    			window.onpopstate = function(event) {
    				$(".l--map--place-wrapper").first().remove();
    				if($(".l--map--place-wrapper").length === 0) {
    					$("body").removeClass('noscroll');
    					$(document).scrollTop(this.scrollPos);	
    				}
    			}
        </script>
	</body>
</html>