		<div class="l--common-page-header">
			<h1><?=isset($map)?"지도 수정하기":"새 지도 만들기"?> </h1>
		</div>
		
		<div class="l--edit" id="l--edit">
			<?php if(!isset($map)) { ?>
			<div class="l--edit--items-block">
				<?php echo form_open_multipart(); ?>
					<label class="l--edit--item-label" for="initialize-kml">KML 파일 불러오기</label>
					<input type="file" name="kml" id="initialize-kml" accept="application/kml" />
					<input type="submit" class="s--button d--block f--do-submit" value="등록">
				</form>				
	
				<div class="l--list-divider horizontal">
					<span class="l--list-divider--label">또는...</span>
				</div>
			</div>
			<?php } ?>
			
			<form method="POST">
				<div class="l--edit--items-block">
					<div class="l--edit--item-block">
						<label class="l--edit--item-label" for="map_name">이름</label>
						<input type="text" id="map_name" placeholder="이 지도의 이름은 무엇인가요?" name="map_name" value="<?=isset($map)?$map->name:""?>" required />
					</div>
					
					<div class="l--edit--item-block">
						<label class="l--edit--item-label" for="map_description">설명</label>
						<textarea class="form-control" id="map_description" placeholder="이 지도에 대한 설명을 써주세요." name="map_description"><?=isset($map)?$map->description:""?></textarea>
					</div>
					
					<div class="l--edit--item-block">
						<label class="l--edit--item-label" for="map_category">지도 카테고리</label>	
						<div class="l--edit--block--radio-wrapper">
						<?php foreach($categories as $category) { ?>
							<input type="radio" name="map_category" id="map_category_<?=$category->id?>" value="<?=$category->id?>" <?=isset($map)&&$category->id==$map->categoryId?"checked":""?> <?=!isset($map) && $category->permalink == "etc" ? "checked" : ""?> />
							<label for="map_category_<?=$category->id?>">
								<i class="glyphicons glyphicons-<?=$category->permalink?>"></i>
								 <?=$category->name?>
							</label>
						<?php } ?>
						</div>
					</div>
					
					<div class="l--edit--item-block">
						<label class="l--edit--item-label">썸네일(미리보기) 사진</label>
						<div class="">
							<input type="file" multiple accept="image/*" id="fileupload" name="photo" />
						</div>
						
						<div class="l--edit--uploads-list">
						<?php
							if( isset($map) && $map->thumbnail ) {
								echo '<div class="l--edit--uploads-photo">';
								echo '  <div class="photo" style="background: url(\''.$map->thumbnailUrl.'\') center / cover no-repeat;"></div>';
								echo '  <input name="photos_filename[]" type="hidden" value="'. $map->thumbnail .'" />';
								echo '  <div class="overlay">삭제?</div>';
								echo '</div>';
							}
						?>
						</div>
					</div>
					
					<div class="l--edit--item-block">
							
						<label class="l--edit--item-label">공개 설정</label>
						<div class="l--edit--block--radio-wrapper">
							<input type="radio" name="map_permission" id="map_permission_1" value="1" <?=(!isset($map) || (isset($map) && $map->permission == 1)) ? 'checked' : ''?> />
							<label for="map_permission_1"><i class="glyphicons glyphicons-globe"></i> 누구나 편집하기</label>
							<input type="radio" name="map_permission" id="map_permission_2" value="2" <?=(isset($map) && $map->permission == 2) ? 'checked' : ''?> />
							<label for="map_permission_2"><i class="glyphicons glyphicons-group">
								</i> 아는 사람끼리 편집하기
								<input type="password" name="map_password" placeholder="비밀번호" value="<?=isset($map)?$map->password:""?>" />
							</label>
							<input type="radio" name="map_permission" id="map_permission_3" value="3" <?=(isset($map) && $map->permission == 3) ? 'checked' : ''?> />
							<label for="map_permission_3"><i class="glyphicons glyphicons-user-lock"></i> 나만 편집하기</label>
						</div>
					</div>
				</div>
			
				<label for="visible-optional-item" class="l--edit--items-block--title">
					추가 정보 설정
					<div class="inverted-triangle">
						<span class="triangle-one"></span>
						<span class="triangle-two"></span>
					</div>
				</label>
				<input type="checkbox" name="visible-optional-item" id="visible-optional-item" class="f--dropdown-checkbox" checked />
				<div class="l--edit--items-block l--edit--optional-item">
					<div class="l--edit--item-block">
						<label class="l--edit--item-label">항목 만들기</label>
						<button type="button" class="s--button" id="f--add-field">항목 추가</button>
						
						<div class="l--edit--fields">
						<?php if( isset($fields) && count($fields) ) { ?>
						<?php     foreach($fields as $field) { ?>
							<div class="l--edit--block--wrapper-relative" id="fld-<?=$field->id?>">
								<input name="field_name[]" type="text" placeholder="정보 이름" value="<?=$field->name?>">
								<input name="field_id[]" type="hidden" value="<?=$field->id?>">
								<button type="button" class="f--remove-field">삭제</button>
							</div>
						<?php     } ?>
						<?php } ?>
						</div>
					</div>
					
					<div class="l--edit--item-block">					
						<label class="l--edit--item-label">카테고리</label>
						<button type="button" class="s--button" id='f--add-index'>추가</button>
						
						<div class="l--edit--indexes">
						<?php if( isset($indexes) && count($indexes) ) { ?>
						<?php     foreach($indexes as $index) { $time = rand() ?>
							<div class="l--edit--block--wrapper-relative" id="pin-<?=$index->id?>" data-target="<?=$time?>">
								<a href="#" class="f--change-icon f--tooltip-trigger">
									<img src="<?=$index->iconUrl?>" data-target="<?=$time?>" height="20" />
									<div class="l--tooltip l--tooltip--pallete">
										<button data-target="<?=$time?>" data-source="pin-blue.png" type="button"><img src="/assets/img/pin-blue.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-green.png" type="button"><img src="/assets/img/pin-green.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-red.png" type="button"><img src="/assets/img/pin-red.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-yellow.png" type="button"><img src="/assets/img/pin-yellow.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-brown.png" type="button"><img src="/assets/img/pin-brown.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-darkgreen.png" type="button"><img src="/assets/img/pin-darkgreen.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-gray.png" type="button"><img src="/assets/img/pin-gray.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-magenta.png" type="button"><img src="/assets/img/pin-magenta.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-mint.png" type="button"><img src="/assets/img/pin-mint.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-orange.png" type="button"><img src="/assets/img/pin-orange.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-pink.png" type="button"><img src="/assets/img/pin-pink.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-purple.png" type="button"><img src="/assets/img/pin-purple.png" width="15" /></button>
										<button data-target="<?=$time?>" data-source="pin-white.png" type="button"><img src="/assets/img/pin-white.png" width="15" /></button>
									</div>
								</a>
								<input name="index_name[]" type="text" placeholder="범례 이름" value="<?=$index->name?>">
								<input name="index_id[]" type="hidden" value="<?=$index->id?>">
								<input name="index_image[]" type="hidden" data-target="<?=$time?>" value="<?=$index->icon?>">
								<button class="f--remove-index" data-target="<?=$time?>">삭제</button>
							</div>
						<?php     } ?>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="l--edit--items-block">
					<input type="submit" class="s--button d--block f--do-submit" value="등록"></div>
				</div>
			</form>
		</div>
		
		<?php 
			if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net")
				include_once(APPPATH.'views/songnae/templates/footer.php');
			else include_once(APPPATH.'views/templates/footer.php');
		?>
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
		<script id="template-new-field" type="x-tmpl-mustache">
			<div class="l--edit--block--wrapper-relative unregistered">
				<input name="field_name[]" type="text" placeholder="전화번호, 영업시간, 후기..."/>
				<input name="field_id[]" type="hidden" value="new"/>
				<button type="button" class="f--remove-field">삭제</button>
			</div>	
		</script>
		<script id="template-new-index" type="x-tmpl-mustache">
			<div class="l--edit--block--wrapper-relative unregistered" data-target="{{timestamp}}">
				<a href="#" class="f--change-icon f--tooltip-trigger">
					<img src="/assets/img/pin-blue.png" data-target="{{timestamp}}" height="20" />
					<div class="l--tooltip l--tooltip--pallete">
						<button data-target="{{timestamp}}" data-source="pin-blue.png" type="button"><img src="/assets/img/pin-blue.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-green.png" type="button"><img src="/assets/img/pin-green.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-red.png" type="button"><img src="/assets/img/pin-red.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-yellow.png" type="button"><img src="/assets/img/pin-yellow.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-brown.png" type="button"><img src="/assets/img/pin-brown.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-darkgreen.png" type="button"><img src="/assets/img/pin-darkgreen.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-gray.png" type="button"><img src="/assets/img/pin-gray.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-magenta.png" type="button"><img src="/assets/img/pin-magenta.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-mint.png" type="button"><img src="/assets/img/pin-mint.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-orange.png" type="button"><img src="/assets/img/pin-orange.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-pink.png" type="button"><img src="/assets/img/pin-pink.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-purple.png" type="button"><img src="/assets/img/pin-purple.png" width="15" /></button>
							<button data-target="{{timestamp}}" data-source="pin-white.png" type="button"><img src="/assets/img/pin-white.png" width="15" /></button>
					</div>
				</a>
				<input name="index_name[]" type="text" placeholder="범례 이름" value="제목 없음" />
				<input name="index_id[]" type="hidden" value="new"/>
				<input name="index_image[]" type="hidden" data-target="{{timestamp}}" value="pin-blue.png"/>
				<button class="f--remove-index" data-target="{{timestamp}}">삭제</button>
			</div>
		</script>
	</body>
</html>