	<body class="<?=isset($classes)?$classes:""?>">
        <input type="checkbox" name="toggle-switch-map-list" id="toggle-switch-map-list" class="f--dropdown-checkbox" />
	    <div class="l--map ios-scroll">
			<div class="l--my">
				<span class="s--portrait" style="background-image:url('<?=$user->services_portrait_o?>')"></span>
				<h3><?=$user->services_name?></h3>
				<h5 class="map"><a href="/users/<?=$user->users_id?>/map"><?=$count['map']?>개</a></h5>
				<h5 class="place active"><a href="/users/<?=$user->users_id?>/place"><?=$count['place']?>개</a></h5>
			</div>
			<div class="l--map--wrapper">
	        	<ul id="l--places" class="l--places">
					<?php foreach($places as $place) { ?>
					<li class="l--places--place" data-name="<?=$place->name?>" data-address="<?=$place->address?>" data-placeid="<?=$place->id?>" data-indexid="<?=$place->indexId?>" data-path="<?=htmlspecialchars($place->path, ENT_QUOTES)?>" data-iconurl="<?=$place->indexIcon?>">
						<a class="l--places--place--wrapper<?=isset($place->photo)?' p--thumbnail-yes':''?>" href="/place/<?=$place->id?>" style="background-image:url('/assets/img/<?=$place->indexIcon!=NULL?$place->indexIcon:'pin-black.png'?>');">
							<?php if(isset($place->photo)) { ?>
							<div class="l--places--place-thumbnail" style="background-image:url('<?=$place->photo?>')"></div>
							<?php } ?>
							<span class="l--places--place-index"><?=$place->indexName?></span>
							<span class="l--places--place-name"><?=$place->name?></span>
							<span class="l--places--place-address"><?=$place->address?></span>
						</a>
					</li>
					<?php } ?>
				</ul>
        	</div>
        </div>
        
        <div class="l--canvas--wrapper">
	        <div id="map-canvas" class="map-canvas"></div>
	        <div class="map-current-location" id="map-current-location"></div>
	        <label for="toggle-switch-map-list" class="return-list"></label>
        </div>
        
        <?php include_once(APPPATH.'views/templates/scripts.php'); ?>
        <script>
			// 지도 초기화
			var mv = new Somemap(),
			    dragging = null,
			    ee = "mouseenter",
			    le = "mouseleave",
			    lj = new List('l--map--wrapper', {
				    valueNames: [
				    	'l--places--place-name',
				    	'l--places--place-address',
				    	'l--places--place-id',
				    	'l--places--place-index'
				    ]
				});
				
			if("ontouchstart" in window) {
			ee = "touchstart", le = "touchend";
			}
			
			mv.initCanvas('map-canvas', {'center': new daum.maps.LatLng(37.551681, 126.991775), 'level': 7})
			  .addMarkersWithPlaces()
			  .initListenersForMapView()
		</script>
	</body>
</html>