		<div class="l--my">
			<span class="s--portrait" style="background-image:url('<?=$user->services_portrait_o?>')"></span>
			<h3><?=$user->services_name?></h3>
			<h5 class="map active"><a href="/users/<?=$user->users_id?>/map"><?=$count['map']?>개</a></h5>
			<h5 class="place"><a href="/users/<?=$user->users_id?>/place"><?=$count['place']?>개</a></h5>
		</div>
		<div class="l--main">
			<div class="l--container">
				<div class="c--cards">
				<?php $colorset = array('#914626', '#F1409F', '#AADE3A', '#2D5CE6', '#5AC1F5', '#21A93C', '#E0B73F', '#EC413B'); ?>
				<?php foreach($maps as $m) { ?>
				<?php if( isset($m['maps']) ) { ?>
				<?php foreach($m['maps'] as $map) { ?>
					<a class="c--card" href="/map/<?=$map->id?>">
						<?php if($map->thumbnail) { ?>
						<div class="c--card-hero" style="background-image:url('<?=$map->thumbnailUrl?>')">
						</div>
						<div class="c--card-meta">
							<h1 class="c--card-title"><?=$map->name?></h1>
						<?php } else { ?>
						<div class="c--card-hero" style="background: <?=$colorset[array_rand($colorset)]?>;">
							<span class="c--card-title"><?=$map->name?></span>
						</div>
						<div class="c--card-meta">
						<?php } ?>
							<h3 class="c--card-creator">
								<span class="s--portrait" <?=$map->services_portrait_s?'style="background-image:url(\''.$map->services_portrait_s.'\')"':''?>></span> <?=$map->services_name?>
							</h3>
							<p class="c--card-desc"><?=$map->description?></p>
						</div>
						<div class="c--card-tail"></div>
					</a>
				<?php }}} ?>
				</div>
			</div>
		</div>
		
		<?php include_once(APPPATH.'views/templates/footer.php'); ?>
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
	</body>
</html>