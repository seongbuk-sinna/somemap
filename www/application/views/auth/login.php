		<div class="l--login">
			
			<a href="/auth/kakao?next=<?=$_GET['next']?>" class="s--button c--kakao d--block">카카오톡으로 로그인</a>
			<a href="/auth/facebook?next=<?=$_GET['next']?>" class="s--button c--facebook d--block">페이스북으로 로그인</a>
			
			<div class="l--list-divider horizontal"></div>
			
			<?php echo $message?'<div class="alert alert-warning">'.$message.'</div>':""; ?>
			
			<?php echo form_open("/auth/login?next=".(isset($_GET['next'])?$_GET['next']:""));?>			
			  <p>
			    <?php echo lang('login_identity_label', 'identity');?>
			    <?php 
				    $identity['placeholder'] = "user@somemap.kr";
				    echo form_input($identity);
				?>
			  </p>
			
			  <p>
			    <?php echo lang('login_password_label', 'password');?>
			    <?php 
					$password['placeholder'] = "**********";
					echo form_input($password);
				?>
			  </p>
			
<!--
			  <p>
			    <?php echo lang('login_remember_label', 'remember');?>
			    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
			  </p>
			
-->
			
			  <p><?php echo form_submit(array("class" => "s--button d--block f--do-submit", "type" => "submit"), lang('login_submit_btn'));?></p>
<!-- 			  <p><button class="btn btn-block btn-info btn-sm"><?php echo lang('create_user_heading'); ?></button></p> -->
			<?php echo form_close();?>
			
<!-- // 			<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p> -->
		</div>