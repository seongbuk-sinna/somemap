		<div class="l--main--hero" style="background-image: url('/assets/img/hero.png');">
			<div class="l--main--hero--wrapper">
				<div class="l--main--hero--text">
					<h1>썸맵으로 모두 다 같이,<br />새로운 지도를 만들어보세요.</h1>
					<a href="/map/edit" class="s--button f--do-submit" style="padding: 12px 25px; margin-top: 20px;font-size:1.2em;">지도 만들기</a>
				</div>
			</div>
		</div>
		
		<div class="l--main--filters">
			<ul>
				<li><a href="/explore/?o=recent">전체</a>
				<?php foreach($categories as $category) { ?>
				<li>
					<a href="/explore/?c=<?=$category->permalink?>" class="<?=(isset($_GET['c'])&&$_GET['c']==$category->permalink)?"selected":""?>"><?=$category->name?></a>
				</li>
				<?php } ?>					
			</ul>
		</div>
		
		<div class="l--main">
				<?php $colorset = array('#914626', '#F1409F', '#AADE3A', '#2D5CE6', '#5AC1F5', '#21A93C', '#E0B73F', '#EC413B'); ?>
				<?php foreach($maps as $m) { ?>
				<div class="c--cards">
					<div class="l--container">
						<h1 class="c--cards-title"><?=$m['conditions']['categoryName']?></h1>
						<?php if( isset($m['maps']) ) { ?>
						<?php foreach($m['maps'] as $map) { ?>
						<a class="c--card" href="/map/<?=$map->id?>">
							<?php if($map->thumbnail) { ?>
							<div class="c--card-hero" style="background-image:url('<?=$map->thumbnailUrl?>')">
								<span class="c--card-category"></span>
							</div>
							<div class="c--card-meta">
								<h1 class="c--card-title"><?=$map->name?></h1>
							<?php } else { ?>
							<div class="c--card-hero" style="background: <?=$colorset[array_rand($colorset)]?>;">
								<span class="c--card-title"><?=$map->name?></span>
							</div>
							<div class="c--card-meta">
							<?php } ?>
								<h3 class="c--card-creator">
									<span class="s--portrait" <?=$map->services_portrait_s?'style="background-image:url(\''.$map->services_portrait_s.'\')"':''?>></span> <?=$map->services_name?> / <?=$map->placecount?>개의 장소
								</h3>
								<p class="c--card-desc"><?=$map->description?></p>
							</div>
							<div class="c--card-tail"></div>
						</a>
						<?php }} ?>
					</div>
				</div>
				<?php } ?>
		</div>
		
		<?php include_once(APPPATH.'views/templates/footer.php'); ?>
		<?php include_once(APPPATH.'views/templates/scripts.php'); ?>
		<?php if($isAdmin === true) { ?>
		<script> $(".c--cards:first-child").sortable({
			items: "a.c--card",
			update: function(event, ui) {
				var qry = '', a = $(".c--cards:first-child a.c--card");
				for(var i = 0, length = a.length; i < length; i++) {
				    qry += a[i].getAttribute('href').substr(5) + "=" + (length - i) + "&";
				}
				
				location.href = '/api/setFeaturedOrder?' + qry;
			}}); </script>
		<?php } ?>
	</body>
</html>