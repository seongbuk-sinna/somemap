		<div class="l--common-footer">
			<div class="l--container">
				<div class="l--common-footer--op">
					<h4><img src="/assets/img/sinna.png" height="30" alt="성북신나" /></h4>
					<h4><img src="/assets/img/logo-si.png" height="30" alt="소셜이노베이션캠프36" /></h4>
					<h4><img src="/assets/img/logo-df.png" height="30" alt="다음세대재단" /></h4>
					<p>
						<a href="https://www.facebook.com/somemap" target="_blank">페이스북 페이지</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="https://www.facebook.com/groups/somemap/" target="_blank">페이스북 썸맵 사용자 그룹</a>
					</p>
					<p>썸맵은 지역 커뮤니티에 유용한 장소들을 함께 만들어가는 지도 서비스입니다. 자발적인 참여자들이 함께 모여 36시간동안 공익적인 사회변화를 가져올 수 있는 아이디어를 실제로 구현하는 <a href="http://sicamp36.org" target="_blank">소셜이노베이션캠프 36</a>을 통해 개발되었습니다. 본 사이트는 <a href="http://daumfoundation.org" target="_blank">다음세대재단</a> <a href="http://itcanus.net/" target="_blank">아이티캐너스 기금</a> 및 카카오 후원을 통해 제작되었습니다.</p>
				</div>
			</div>
		</div>