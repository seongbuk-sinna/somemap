<!DOCTYPE html>
<html lang="ko" class="<?=isset($classes)?$classes:""?>">
	<head>
		<link href="/assets/typeface/NotoKR-Regular/stylesheet.css" rel="stylesheet" type="text/css">
		<title><?=isset($meta)?$meta['title']." :: 썸맵":"썸맵"?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
		<meta property="og:title" content="<?=isset($meta)?$meta['title']:"썸맵"?>">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?=current_url()?>">
		<meta property="og:image" content="<?=isset($meta['thumbnailUrl'])&&$meta['thumbnailUrl'] !== ''?$meta['thumbnailUrl']:base_url('assets/img/front-bg.jpg')?>">
		<meta property="og:site_name" content="썸맵">
		<meta property="og:description" content="<?=isset($meta)?$meta['description']:"우리 동네 그 장소, 그 골목! 친구들과 함께 의미있는 지도를 만들어보세요."?>">
		<link href="/assets/style.css" rel="stylesheet" type="text/css">
		<link href="/assets/photoswipe/photoswipe.css" rel="stylesheet" type="text/css">
		<link href="/assets/photoswipe/default-skin/default-skin.css" rel="stylesheet" type="text/css">
		<link href="//s3.ap-northeast-2.amazonaws.com/somemap-live-seoul/static/js/owl.carousel.css" rel="stylesheet" type="text/css">
	</head>