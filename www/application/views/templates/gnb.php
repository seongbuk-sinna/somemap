	<body class="<?=isset($classes)?$classes:""?>">
		<div class="l--common-gnb">
    		<div class="l--common-gnb--firstline">
		        <a href="/" class="logo">썸맵</a>
				<div class="l--filters">
					<div class="l--filters--search">
						<form method="GET" action="/explore/">
							<input type="text" name="q" value="" placeholder="" />
							<input type="hidden" name="o" value="<?=isset($_GET['o'])?$_GET['o']:'recent'?>" />
							<input type="hidden" name="c" value="<?=isset($_GET['c'])?$_GET['c']:'all'?>" />
							<input type="submit" value="Go" />
						</form>
					</div>
				</div>
				
				<?php if(isset($session['user_id'])) { ?>
	    		<?php if(count($flags) > 0) { ?>
	    		<!--label for="notification-menu" class="f--dropdown-switch notification login">
	    			<span class="s--noti-badge"><?=count($flags)?></span>
	    		</label>
		        <input type="checkbox" name="notification-menu" id="notification-menu" class="f--dropdown-checkbox" />
	    		<ul class="l--list notification">
					<?php foreach($flags as $flag) { ?>
					<li>
						<a href="/place/<?=$flag->id?>/edit">
							<div class="nm-meta"><?=$flag->mapName?></div>
							<div class="nm"><b><?=$flag->placeName?></b>의 정보 수정 제안이 등록되었습니다.</div>
							<div class="flags-date"><?=$flag->createDate?></div>
						</a>
					</li>
					<?php } ?>
		        </ul-->
		        <?php } ?>
	    		
	    		<label for="portrait-menu" class="f--dropdown-switch portrait">
		    		<span class="s--portrait" <?=isset($session['services_portrait_s']) ? 'style="background-image:url(\''.$session['services_portrait_s'].'\')"' : '' ?>></span>
		    	</label>
		        <input type="checkbox" name="portrait-menu" id="portrait-menu" class="f--dropdown-checkbox" />
	    		<ul class="l--list personal l--list--toggle">
					<li>
						<a href="/users/<?=$session['user_id']?>/map">내 지도</a>
					</li>
					<li>
						<a href="/users/<?=$session['user_id']?>/place">내 장소</a>
					</li>
					<li>
						<a href="/auth/logout?next=<?=htmlspecialchars(current_url())?>">로그아웃</a>
					</li>
		        </ul>
	    		
		        <?php } else { ?>
		        <a href="/auth/login?next=<?=current_url()?>" class="request-login">로그인</a>
		        <?php } ?>
	        </div>
	        
	        <div class="l--common-gnb--secondline">
		        <ul class="l--list l--list-hor common">
<!-- 					<li><a href="/help">도움말</a></li> -->
					<li><a href="https://www.facebook.com/somemap" target="_blank">페이스북</a></li>
					<li><a href="https://www.facebook.com/groups/somemap/" target="_blank">포럼</a></li>
					<li><a href="/help/notice">공지</a></li>
					<?php if(isset($session['user_id'])) { ?>
					<li>
						<a href="/map/edit">지도 만들기</a>
					</li>
					<?php } ?>
		        </ul>
	        </div>
    	</div>