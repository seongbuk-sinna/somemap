<?php if (!defined('BASEPATH')) die();

class Api extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		header('Content-Type: application/json');
		$this->result = array('result' => false, 'errorCode' => null, 'body' => null);
	}
	
	public function maps($mapId = NULL) {
		if($mapId === NULL) {
			$c['pageSize'] = 24; // 1회에 불러올 지도의 수
			$c['currentNumberOfPage'] = (int) $this->input->get_post('page'); // 현재 페이지
			if( !$c['currentNumberOfPage'] || $c['currentNumberOfPage'] < 1 ) {
				$c['currentNumberOfPage'] = 1; // 입력되지 않았거나 1보다 작을때 1로 보정
			}
			$c['orderType'] = $this->input->get_post('order') ? $this->input->get_post('order') : 'best'; // 정렬 기준
			$c['category'] = $this->input->get_post('category'); // 지도 카테고리
			$c['isOpen'] = 'Y';
			
			// Map Model을 통해 가져옵니다.
			$this->result['body'] = $this->Map_model->getMaps($c);
			
			if($this->result['body'] !== null) {
				$this->result['result'] = true;
			}
			
			return $this->output->set_output(json_encode($this->result));	
		} else if($mapId !== NULL) {
			// 지도 기본 정보를 Map Model로부터 가져옵니다.
			$map = $this->Map_model->getMap($mapId);
			
			// ID에 해당하는 지도가 없을 때
			if($map === NULL) {
				$this->result['errorCode'] = 100;
				exit(json_encode($this->result));
			} else {
				// 지도 조회시 hit 카운트 +1
				$this->db->where('id', $mapId)->update('map', array('hit' => $map->hit + 1));
			
				$places = $this->Place_model->getPlaces($mapId); // 장소 목록을 가져옵니다.
				$indexes = $this->Map_model->getIndexes($mapId); // 범례 목록을 가져옵니다.	

				$this->result['result'] = true;
				$this->result['body'] = array(
					'map' => $map,
					'places' => $places,
					'indexes' => $indexes
				);
			
				return $this->output->set_output(json_encode($this->result));
			}
		}
	}

	// 하나의 장소 정보를 되돌립니다
	/**
	 * @param int $placeId 장소의 고유 ID
	 * @return json 기본 API 규격으로 리턴
	 */
	public function getPlace()
	{		
		$placeId = $this->input->get_post('placeId');
		// 장소 아이디가 입력되지 않았을 때 종료합니다.
		if(!$placeId) { 
			$this->result['errorCode'] = 100;
			exit(json_encode($this->result));
		}
		
		$place = $this->Place_model->getPlace($placeId);
		$place->fieldData = $this->Place_model->getPlaceFieldData($placeId);		
		 
		return $this->output->set_output( json_encode($place) );
	}
	
	public function changePlaceOrder() {
		$this->result['result'] = true;
		$this->result['body'] = array(
			'inputPlace' => $this->Place_model->changePlaceOrder( $this->input->post('p') )
		);
		
		return $this->output->set_output( json_encode($this->result) );
	}
	
	public function comments($method = null) {
		if(!isset($this->session->user_id)) {
			$this->result['errorCode'] = 100;
			return $this->output->set_output( json_encode($this->result) );
		}
		
		if($method == 'new') {
			$this->db->insert('comment', array(
				'placeId' => $this->input->post('placeId'),
				'content' => htmlspecialchars($this->input->post('content')),
				'userId' => $this->session->user_id
			));
		}
	}
	
	// 지도의 사진 업데이트용
	/**
	 * @param int $mapId 사진 업로드 할 지도 id
	 * @param int $placeId 사진 업로드 할 장소 id
	 * @param data $image 업로드할 실제 이미지
	 * @return json 기본 API 규격의 body 안에 url로 이미지의 url 과 map_photo의 seq 전달
	 */
	public function uploadPhoto()
	{
		// 이미지 저장 경로
		$uploadPath = 'uploads/';
		$uploadThumbPath = $uploadPath.'thumbs/';
		
		// 업로드 설정	
		$this->load->library('upload', array(
			'upload_path'   => $uploadPath,
			'allowed_types' => 'gif|jpg|jpeg|png',
			'max_size'      => '20480',
			'encrypt_name'  => TRUE
		));

		if($this->upload->do_upload('photo')) {
			// 이미지 파일 업로드 처리 후 관련 정보 가져오기
			$file = $this->upload->data();
			
			$this->load->library('image_lib');
			
			$exif = @exif_read_data($uploadPath . $file['file_name']);
			if(!empty($exif['Orientation'])) {
				$oris = array();			
				switch($exif['Orientation']) {
			        case 2: $oris = array('hor'); break; // horizontal flip
			        case 3: $oris = array('180'); break; // 180 rotate left
			        case 4: $oris = array('ver'); break; // vertical flip
					case 5: $oris = array('ver', '270'); break; // vertical flip + 90 rotate right
					case 6: $oris = array('270'); break; // 90 rotate right
			        case 7: $oris = array('hor', '270'); break; // horizontal flip + 90 rotate right
			        case 8: $oris = array('90'); break; // 90 rotate left
			        default: break;
				}
				
				$config['image_library'] = 'gd2';
				$config['source_image']	= $uploadPath . $file['file_name'];	
				
				foreach ($oris as $ori) {
					$config['rotation_angle'] = $ori;
					$this->image_lib->initialize($config); 
					$this->image_lib->rotate();
				}	
			}
			
			$this->image_lib->initialize( array(
				'source_image'   => $uploadPath.$file['file_name'],
				'new_image'      => $uploadThumbPath,
				'maintain_ratio' => TRUE,
				'width'          => 500,
				'height'         => 500 )
			);
			$this->image_lib->resize();
			
			$this->load->library('aws');
			$this->aws->load('s3');
			$response = $this->aws->s3->putObject([
				'Bucket' => 'somemap-live-seoul', 
				'SourceFile'   => $uploadPath.$file['file_name'],
				'Key'    => $uploadPath.$file['file_name']
			]);
			
			$response_thumbnail = $this->aws->s3->putObject([
				'Bucket' => 'somemap-live-seoul',
				'SourceFile'   => $uploadThumbPath.$file['file_name'],
				'Key'    => $uploadThumbPath.$file['file_name']
			]);
			
			if($response['ObjectURL'] && $response_thumbnail['ObjectURL']) {
				unlink($uploadPath.$file['file_name']);
				unlink($uploadThumbPath.$file['file_name']);
				
				$this->result['result'] = true;
				$this->result['body'] = array(
					'msg' => '업로드가 완료되었습니다.',
					'filename' => $file['file_name'],
					'url' => $response_thumbnail['ObjectURL']
				);
			} else {
				$this->result['errorCode'] = 100;
				$this->result['body'] = array('msg' => '저장에 실패합니다.');
			}
		} else {
			$this->result['errorCode'] = 100;
			$this->result['body'] = $this->upload->display_errors();
		} return $this->output->set_output(json_encode($this->result));
	}

	// 지도의 필드 또는 하위구분 삭제용
	/**
	 * @param int $something_id 지도 필드나 하위 구분의 seq
	 * @return json 기본 API 규격으로 리턴
	 */
	public function deleteFields()
	{
		$something_id = explode("-", $this->input->post('something_id'));
		
		if(!$this->input->post('something_id')) {
			// 잘못된 값이 입력됐을 경우
			$this->result['errorCode'] = 100;
			$this->result['body'] = "입력된 값이 없습니다.";
			exit(json_encode($this->result));
		}
		
		if(!($something_id[0] == "fld" || $something_id[0] == "pin")) {
			// 잘못된 값이 입력됐을 경우
			$this->result['errorCode'] = 300;
			$this->result['body'] = "잘못된 값이 입력되었습니다.";
			exit(json_encode($this->result));
		}
		
		if($something_id[0] == "fld") {
			// 필드 데이터 삭제
			$this->db->where('fieldId', $something_id[1]);
			$this->db->delete('field_data');

			// 필드 삭제
			$this->db->where('id', $something_id[1]);
			$this->db->delete('field');

			$this->result['result'] = true;
		} else if ($something_id[0] == "pin") {
			// 세부구분 데이터 삭제
			$this->db->where('indexId', $something_id[1]);
			$this->db->delete('place');

			// 세부구분 삭제
			$this->db->where('id', $something_id[1]);
			$this->db->delete('index');

			$this->result['result'] = true;
		}

		return $this->output->set_output(json_encode($this->result));
	}
	
	public function youtube() {
		if(!isset($_GET['url']) || !$_GET['url']) {
			$this->result['errorCode'] = 300;
			$this->result['body'] = "주소를 입력해주세요.";
			exit(json_encode($this->result));
		}
		
		$handle = curl_init($_GET['url']);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
		/* Get the HTML or whatever is linked in $url. */
		$response = curl_exec($handle);
		
		/* Check for 404 (file not found). */
		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		
		if($httpCode == 200) {
			preg_match('/([a-zA-Z0-9\-\_]+){10,}/', $_GET['url'], $matches);
			$this->result['result'] = true;
			$this->result['body'] = array(
				'key' => $matches[0],
				'embed' => 'https://www.youtube.com/embed/'.$matches[0],
				'thumbnail' => 'http://img.youtube.com/vi/'.$matches[0].'/0.jpg'
			);
		} else {
			$this->result['errorCode'] = 404;
			$this->result['body'] = "Not exist video";
			exit(json_encode($this->result));
		}
		
		curl_close($handle);
		return $this->output->set_output(json_encode($this->result));
	}
	
	public function s3test() {
		$this->load->library('aws');
		$this->aws->load('s3');
		print_r($this->aws->s3->getObject([
		    'Bucket' => 'somemap-live-seoul',
		    'Key'    => 'uploads/001f929391f09eba81c49e09f00ca0c3.jpg'
		]));
	}
	
	public function setFeaturedOrder() {
		if($this->ion_auth->is_admin()) {
			foreach($this->input->get() as $key => $val) {
				$this->db->where('id', $key)->set('featured', $val)->update('map');
			}
		}
		
		redirect('/');
	}
}

/* End of file api.php */
/* Location: ./application/controllers/api.php */