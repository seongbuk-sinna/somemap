<?php if (!defined('BASEPATH')) die();

class Help extends CI_Controller {
	public function index() {
		$this->load->helper('form');
		$this->load->library('form_validation');
			
		$data['flags'] = $this->Place_model->getFlags();
		$data['session'] = $this->session->userdata();
		
		$this->load->library('ion_auth');
		if($this->ion_auth->is_admin()) $data['isAdmin'] = true;
		else $data['isAdmin'] = false;
		
		$this->form_validation->set_rules('title', 'title', 'required');
		
		if($this->form_validation->run() == true && $data['isAdmin'] == true) {
			$input = array('title' => $this->input->post('title'), 'content' => $this->input->post('content'), 'writerId' => $this->session->user_id);
			$this->db->insert('notice', $input);
			if($this->db->insert_id()) redirect('/help');
		} else {
			$data['notices'] = $this->db->get('notice')->result();
			$this->load->view('templates/html_head', $data);
			$this->load->view('templates/gnb', $data);
			$this->load->view('help', $data);		
		}
	}
	
	public function notice() {
		$data['flags'] = $this->Place_model->getFlags();
		$data['session'] = $this->session->userdata();
		
		$this->load->view('templates/html_head', $data);
		$this->load->view('templates/gnb', $data);
		$this->load->view('notice');
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */ 