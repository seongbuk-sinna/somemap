<?php if (!defined('BASEPATH')) die();

class Explore extends CI_Controller {
	public function index($keyword = null) {
		$data['categories'] = $this->Map_model->getCategories();
		$data['flags'] = $this->Place_model->getFlags();
		$data['session'] = $this->session->userdata();
		
		
		$c['currentNumberOfPage'] = 1; // 현재 페이지
		$c['category'] = 'all'; // 지도 카테고리
		$c['isOpen'] = 'Y';
		$c['pageSize'] = 0;
		$c['orderType'] = 'recent';
			
		if($this->input->get('q')) $c['keyword'] = $this->input->get('q');
		if($this->input->get('c')) { 
			$c['category'] = $this->input->get('c'); 
			$category = $this->db->select('name')->get_where('category', array('permalink' => $this->input->get('c')))->row();
			$c['categoryName'] = isset($category) ? $category->name : ""; }
		if($this->input->get('o')) $c['orderType'] = $this->input->get('o');
		
		$maps = $this->Map_model->getMaps($c);
		
		if(substr($this->input->get('q'), 0, 1) === '#') {
			foreach($maps['maps'] as $x => $map) {	
				preg_match('/^'.$this->input->get('q').'(?<=[^가-힣\w])|'.$this->input->get('q').'(?=[^가-힣\w])|'.$this->input->get('q').'$/', $map->description, $matches);
				if(count($matches) === 0) unset($maps['maps'][$x]);
			}
		} 
		
		$data['maps']['selected'] = $maps;	
		
		$this->load->view('templates/html_head', $data);
		$this->load->view('templates/gnb', $data);
		$this->load->view('explore', $data);	
	}
}

/* End of file search.php */
/* Location: ./application/controllers/Explore.php */ 