<?php if (!defined('BASEPATH')) die();

class Map extends CI_Controller {

  	public function index() {
	    if(! ($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net")) { redirect('/'); }
	
	    $c['pageSize'] = 24; // 1회에 불러올 지도의 수
			$c['currentNumberOfPage'] = 1; // 현재 페이지
			$c['orderType'] = 'best'; // 정렬 기준
			$c['category'] = 'all'; // 지도 카테고리
			$c['isOpen'] = 'Y';
			
			$data = array(
				'session' => $this->session->userdata(),
				'classes' => 'songnae main',
				'menus' => $this->db->order_by('me_order, me_id')->get_where('g5_menu', array('me_use' => '1', 'length(me_code)' => 2))->result(),
				'categories' => $this->Map_model->getCategories(),
				'data' => $this->Map_model->getMaps($c)
			);
			
			$this->load->view('songnae/templates/html_head', $data);
			$this->load->view('songnae/templates/gnb', $data);
			$this->load->view('songnae/main', $data);
	}

	public function view($mapId = null) {
		$map = $this->Map_model->getMap($mapId);
		if( !$map ) redirect('/');
		$meta['title'] = $map->name;
		$meta['description'] = isset($map->description) ? $map->description : '';
		$meta['thumbnailUrl'] = isset($map->thumbnailUrl) ? $map->thumbnailUrl : '';
		$map->thumbnailUrl = isset($map->thumbnailUrl) ? $map->thumbnailUrl : '';
		
		$this->load->model('User_model');
		
		$data = array(
			'session' => $this->session->userdata(),
			'map' => $map,
			'places' => $this->Place_model->getPlaces(array('mapId' => $map->id)),
			'indexes' => $this->Map_model->getIndexes($map->id),
			'creator' => $this->User_model->getUser($map->userId),
			'categories' => $this->Map_model->getCategories(),
			'meta' => $meta
		);
		
		$this->load->library('ion_auth');
		if($this->ion_auth->is_admin()) $data['isAdmin'] = true;
		else $data['isAdmin'] = false;
		$data['flags'] = $this->Place_model->getFlags();
		
		if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net") {
			$data['classes'] = 'songnae map';
			$data['menus'] = $this->db->order_by('me_order, me_id')->get_where('g5_menu', array('me_use' => '1', 'length(me_code)' => 2))->result();
	
			$this->load->view('songnae/templates/html_head', $data);
			$this->load->view('songnae/templates/gnb', $data);
			$this->load->view('map/view', $data);
		} else {
			$data['classes'] = 'map';
			$this->load->view('templates/html_head', $data);
			$this->load->view('templates/gnb', $data);
			$this->load->view('map/view', $data);
		}
	}
	
	/**
	 * 지도를 생성하거나 정보를 편집합니다.
	 * @param $mapId 입력되어 있다면 '수정', 입력되어 있지 않으면 '생성'
	 */
	public function edit($mapId = null) {
		$this->_lc();
		
		// 폼 검증
		// Todo: 서버 사이드 폼 검증 보완할 것
		$this->form_validation->set_rules('map_name', 'Map-Name', 'required');
		
		if($this->form_validation->run() == true) {
		// 값이 입력된 상태에서 데이터 처리를 진행합니다.
			if($mapId) $mapId = $this->Map_model->editMap($this->input->post(), $mapId); // 수정
			else $mapId = $this->Map_model->editMap($this->input->post()); // 생성
			
			if($mapId) {
				// 제대로 처리되었으면 창을 닫고 지도로 이동합니다.
				redirect('/map/'.$mapId);
			} else {
				echo '<script type="text/javascript">alert("실패했습니다!"); history.back();</script>';
			}
		} else if(isset($_FILES['kml']) && $_FILES['kml']['tmp_name']) {
			$xml = file_get_contents($_FILES['kml']['tmp_name']);
			$xml = str_replace(array("\n", "\r", "\t"), '', $xml);
			$xml = preg_replace('/<img.+?>/i', '', $xml);
			$xml = trim(str_replace('"', "'", $xml));
			$xml = simplexml_load_string($xml, null, LIBXML_NOCDATA);

			$args = '';
			$args['map_name'] = $xml->Document->name."";
			$args['map_description'] = preg_replace('/<br\s*\/?>|<br>/', "\n", $xml->Document->description."");
			$args['map_permission'] = 1;
			
			$count = 0;
			foreach($xml->Document->Folder as $folder) {
				$args['index_id'][$count] = 'new';
				$args['index_name'][$count] = $folder->name;
				$args['index_image'][$count++] = 'pin-blue.png'; 
			}
			
			$mapId = $this->Map_model->editMap($args, 0);
			$indexes = $this->Map_model->getIndexes($mapId);
			$count = 0;
			foreach($xml->Document->Folder as $folder) {
				if(isset($folder->Placemark) && count($folder->Placemark)) {
					foreach($folder->Placemark as $place) {
						$args = '';
						$args['name'] = $place->name."";
						$args['description'] = preg_replace('/<br\s*\/?>|<br>/', "\n", $place->description."");
						$latlng = explode(',', $place->Point->coordinates);
						$args['path'] = json_encode(array(array("lat" => $latlng[1], "lng" => $latlng[0])));
						$args['type'] = 1;
						$args['indexId'] = $indexes[$count]->id;
						$args['address'] = '';
						$args['mapId'] = $mapId;
						if(isset($place->ExtendedData->Data) && count($place->ExtendedData->Data)) {
							$args['photos_filename'] = array();
							foreach($place->ExtendedData->Data->value as $data) {
								array_push($args['photos_filename'], $data."");
							}
						}
						
						$this->Place_model->editPlace($args, 'create');
					}
					$count++;
				}
			}
			
			redirect('/map/'.$mapId);
			
		} else {
			// 값이 입력되지 않았으면 폼을 출력합니다.
			if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net") {
				$this->load->view('songnae/templates/html_head');
				$this->load->view('songnae/templates/gnb', array(
					'classes' => 'songnae edit-data', 
					'session' => $this->session->userdata(),
					'categories' => $this->Map_model->getCategories(false),
					'flags' => $this->Place_model->getFlags()
				));
			} else {
				$this->load->view('templates/html_head');
				$this->load->view('templates/gnb', array(
					'classes' => 'edit-data',
					'session' => $this->session->userdata(),
					'categories' => $this->Map_model->getCategories(false),
					'flags' => $this->Place_model->getFlags()
				));
			}
				
			if($mapId) {
				// 수정
				$map = $this->Map_model->getMap($mapId);
				$this->load->library('ion_auth');
				if($map->userId === $this->session->user_id || $this->ion_auth->is_admin()) {
					$this->load->view('map/edit', array(
							'map' => $map,
							'fields' => $this->Map_model->getFields($mapId),
							'indexes' => $this->Map_model->getIndexes($mapId)
						)
					);	
				} else {
					echo '<script type="text/javascript">alert("수정할 수 없습니다."); history.back(); </script>';
				}
			} else {
				// 생성
				$this->load->view('map/edit');	
			}
		}
	}
	
	public function remove($mapId = null) {
		if(!$mapId) redirect("/");
		if( !$this->session->userdata('user_id') ) {
			exit('<meta charset="utf-8" /><script>alert("로그인 후 이용 가능합니다."); history.back();</script>');
		}
		
		$map = $this->Map_model->getMap($mapId);
		$this->load->library('ion_auth');
		
		if ($this->session->user_id == $map->userId || $this->ion_auth->is_admin()) {
			$this->db->where('id', $mapId)->update('map', array('isOpen' => 'N'));
			redirect("/explore");
		} else {
			echo '<meta charset="utf-8" /><script>alert("삭제할 수 없습니다.");history.back();</script>';
		}
		
	}
	
	public function featured($mapId = null, $command = null) {
		if($mapId == null || $command == null || !$this->ion_auth->is_admin()) redirect('/');
		else {
			if($command == "enable") $fVal = 1;
			else if($command == "disable") $fVal = 0;
			$this->db->set('featured', $fVal)->where('id', $mapId)->update('map');
			redirect('/map/'.$mapId);
		}
	}
	
	/**
	 * 특정 지도의 이미지 추출
	 * @param $map_id map id
	 */
	public function exportImage($map_id) {
		$this->load->model("map_model");
		
		// get map info
		$map_array = $this->map_model->getMapById($map_id);
		
		if (count($map_array) == 0) {
			return;
		}
		
		$map_name = $map_array->map_name;
		$zip_filename = $map_name.".zip";
		
		// images
		$this->_export_excel_and_images_in_zip($map_id, $zip_filename, NULL, NULL);
	}
	
	/**
	 * 특정 지도의 엑셀 데이터와 이미지 추출
	 * @param $map_id map id
	 */
	public function exportExcelAndImage($map_id) {
		$this->load->model("map_model");
		
		// get map info
		$map_array = $this->map_model->getMapById($map_id);
		
		if (count($map_array) == 0) {
			return;
		}
		
		$map_name = $map_array[0]["map_name"];
		$zip_filename = $map_name.".zip";
		
		// excel
		$map_data_array = $this->map_model->getMapDataForExcelExport($map_id);
		
		$excel_contents = $this->_get_exported_excel_content($map_data_array, $map_name);
		
		$excel_filename = "map_".$map_id.".xls";
		
		// images
		$this->_export_excel_and_images_in_zip($map_id, $zip_filename, $excel_filename, $excel_contents);
		
	}
	
	/**
	 * 엑셀로 데이터 추출
	 * @param unknown $map_id
	 */
	public function exportExcel($mapId) {
		$map = $this->Map_model->getMap($mapId);
		if (!$map) { return; }
		else {
			$places = $this->Place_model->getPlaces(array('mapId' => $mapId, 'isExport' => true));
			$this->_export_excel_and_download($places, "map_".$mapId."_".$map->name.".xls", $map->name);	
		}
	}
	
	/**
	 * excel 파일과 이미지를 zip 으로 묶어서 내보냄
	 * @param $map_id
	 * @param $zip_filename
	 * @param string $add_filename
	 * @param string $add_file_contents
	 */
	private function _export_excel_and_images_in_zip($map_id, $zip_filename, $excel_filename = NULL, $excel_file_contents = NULL) {
		$this->load->library('zip');
		
		$map_photo_array = $this->map_model->getMapPhotoByMap($map_id);
		
		$prev_map_data_name = "";
		$number = 1;
		foreach ($map_photo_array as $row) {
			if ($prev_map_data_name != $row["map_data_name"]) {
				$prev_map_data_name = $row["map_data_name"];
				$number = 1;
			}
			
			$org_filename = $row["photo_path"];
			$file_parts = pathinfo($org_filename);
				
			$org_filepath = $this->_getPhotosUrl($org_filename);
			
// 			if (file_exists($org_filepath)) {
// 				echo $org_filepath."<br /><br />";
				$save_filename = "images";
				$save_filename .= "/";
				$save_filename .= iconv("UTF-8", "EUC-KR", $row["map_data_name"]);
				$save_filename .= "/";
				$save_filename .= $number;
				$save_filename .= ".".$file_parts['extension'];
		
				$this->zip->read_file($org_filepath, $save_filename);
				
				$number++;
// 			} else {
				// echo "file not exists";
// 			}
		}

		// 
		if ($excel_filename != NULL && $excel_file_contents != NULL) {
			// add excel data
			$this->zip->add_data($excel_filename, $excel_file_contents);
		}
		
		$this->zip->archive($zip_filename);
		$this->zip->download($zip_filename);
	}
	
	private function _export_images_in_zip($map_id, $zip_filename) {
		$this->_export_excel_and_images_in_zip($map_id, $zip_filename, NULL, NULL);
	}
		
	/**
	 * array 내용을 excel 로 출력 후 $filename 으로 저장
	 * @param array $array excel 로 저장할 array data
	 * @param string $filename 저장 할 파일명 
	 */
	private function _export_excel_and_download($array, $filename, $title = NULL) {
		$this->load->library('excel');
		if ($title != NULL) {
			$this->excel->getActiveSheet()->setTitle($title);
		}
		
		// header
		$h = array ();
		foreach ( $array as $row ) {
			foreach ( $row as $key => $val ) {
				if (! in_array ( $key, $h )) {
					$h [] = $key;
				}
			}
		}
		
		$column = 0;
		foreach ( $h as $key ) {
			$key = ucwords ( $key );
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $key);
			$column++;
		}
		
		// body
		$row_num = 0;
		foreach ( $array as $row ) {
			$column_num = 0;
			foreach ( $row as $val ) {
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($column_num, $row_num + 2, $val);
				$column_num++;
			}
			$row_num++;
		}
		
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	private function _get_exported_excel_content($array, $title = NULL) {
		$this->load->library('excel');
		if ($title != NULL) {
			$this->excel->getActiveSheet()->setTitle($title);
		}
	
		// header
		$h = array ();
		foreach ( $array as $row ) {
			foreach ( $row as $key => $val ) {
				if (! in_array ( $key, $h )) {
					$h [] = $key;
				}
			}
		}
	
		$column = 0;
		foreach ( $h as $key ) {
			$key = ucwords ( $key );
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $key);
			$column++;
		}
	
		// body
		$row_num = 0;
		foreach ( $array as $row ) {
			$column_num = 0;
			foreach ( $row as $val ) {
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($column_num, $row_num + 2, $val);
				$column_num++;
			}
			$row_num++;
		}
	
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$filepath = "tmp_export/temp.xls";
		$objWriter->save($filepath);
		
		$file_contents = file_get_contents($filepath);
		unlink($filepath);
		return $file_contents;
	}
	
	public function _lc() {
		if( !$this->session->user_id ) { 
			header("Location: /auth/login?next=".base_url($_SERVER['REQUEST_URI'])); 
			exit();
		} else {
			$this->load->helper('form');
			$this->load->library('form_validation');
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/Map.php */