<?php if (!defined('BASEPATH')) die();

class Main extends CI_Controller {
	public function index() {
		$data['categories'] = $this->Map_model->getCategories();
		$data['flags'] = $this->Place_model->getFlags();
		$data['session'] = $this->session->userdata();
		
		if($this->ion_auth->is_admin()) $data['isAdmin'] = true;
		else $data['isAdmin'] = false;
		
		$this->load->library('ion_auth');
		
		$c['currentNumberOfPage'] = 1; // 현재 페이지
		$c['category'] = 'all'; // 지도 카테고리
		if(!$this->ion_auth->is_admin()) $c['isOpen'] = 'Y';
		
		// 검색 쿼리가 있는 경우
		if($this->input->get('q')) {
			$c['keyword'] = $this->input->get('q');
			$c['pageSize'] = 0;
			$c['orderType'] = 'recent';
			$data['maps']['search'] = $this->Map_model->getMaps($c);
		} else if(!$this->input->get('c') && !$this->input->get('o')) {
			// 쿼리와 카테고리 설정이 없는 경우
			$c['categoryName'] ='썸맵 스태프 추천';
			$c['pageSize'] = 19; // 1회에 불러올 지도의 수
			$c['orderType'] = $this->input->get('o') ? $this->input->get('o') : 'recent'; // 정렬 기준
			$c['featured'] = true;
			$c['whereIn'] = null;
			$data['maps']['feature'] = $this->Map_model->getMaps($c);
			$c['featured'] = false;
			$c['categoryName'] = '지도 전체 보기';
			$data['maps']['best'] = $this->Map_model->getMaps($c);	
		} else {
			// 그 외
			$c['category'] = $this->input->get('c');
			$c['orderType'] = $this->input->get('o');
			$c['pageSize'] = 100;
			$data['maps']['selected'] = $this->Map_model->getMaps($c);
		}
		
		$this->load->view('templates/html_head', $data);
		$this->load->view('templates/gnb', $data);
		
		$data['mc'] = $this->db->count_all('map');
		$data['pc'] = $this->db->count_all('place');
		$this->load->view('main', $data);	
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */ 