<?php if (!defined('BASEPATH')) die();

class Place extends MY_Controller {
	
	public function view($placeId = null, $simplify = null) {
		$place = $this->Place_model->getPlace($placeId);
		$place->fieldData = $this->Place_model->getPlaceFieldData($placeId);
		
		$this->load->library('ion_auth');
		if($this->ion_auth->is_admin()) $data['isAdmin'] = true;
		else $data['isAdmin'] = false;
		
		if($simplify == "simplify") {
			$data['place'] = $place;
			$data['session'] = $this->session->userdata();
			$this->load->view('place/place', $data);
		} else {
			$meta['title'] = $place->name;
			$meta['description'] = $place->description;
			if(isset($place->photos) && count($place->photos)) $meta['thumbnailUrl'] = $place->photos[0]['thumb_url'];
			
			$data['session'] = $this->session->userdata();
			$data['flags'] = $this->Place_model->getFlags();
			$data['place'] = $place;
			$data['meta'] = $meta;
			$data['categories'] = $this->Map_model->getCategories();
			
			if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net") {
				$prefix = 'songnae';
			}
			
			$data['classes'] = 'place'.(isset($prefix)?' '.$prefix:'');
			$this->load->view((isset($prefix)?$prefix.'/':'').'templates/html_head', $data);
			$this->load->view((isset($prefix)?$prefix.'/':'').'templates/gnb', $data);	
				
			$this->load->view('place/place', $data);
			$this->load->view('place/view', $data);	
		}
	}
	
	public function add($mapId = null) {
		if(	$mapId == null ) { header("/"); }
		$this->_lc();
		
		$map = $this->Map_model->getMap($mapId);
		
		if($this->form_validation->set_rules('name', 'name', 'required')->run() == true) {
			if(($map->permission == 2 && $map->password == $this->input->post('map_password')) || ($map->permission == 3 && $map->userId == $this->session->user_id) || $map->permission == 1) {
				$args = $this->input->post();
				$args['mapId'] = $mapId;
				if( $this->Place_model->editPlace($args, 'create') ) 
					redirect('/map/'.$mapId);	
			}
			
			echo '<script type="text/javascript">alert("등록에 실패했습니다!");history.back();</script>';	
			
		} else {
			$this->result['map'] = $map;
			$this->result['fields'] = $this->Map_model->getFields($mapId);
			$this->result['indexes'] = $this->Map_model->getIndexes($mapId);
			
			if($map->permission == 1 || $map->permission == 2 || (($map->permission == 3) && $map->userId == $this->session->user_id) ) {
				$this->_render('place/edit');
			} else {
				redirect('/');
			}
		}
	}
	
	public function edit($placeId = null) {
		if( $placeId == null) { header("/"); }
		$this->_lc();
		
		$place = $this->Place_model->getPlace($placeId);
		$this->load->library('ion_auth');
		if($this->form_validation->set_rules('name', 'name', 'required')->run() == true) {
			$args = $this->input->post();
			$args['placeId'] = $placeId;
			if( $this->Place_model->editPlace($args, 'modify') ) 
				redirect('/map/'.$place->mapId);	
			else 
				echo '<script type="text/javascript">alert("등록에 실패했습니다!");history.back();</script>';
		} else {
			$this->result['map']         = $this->Map_model->getMap($place->mapId); // 지도 기본 정보 로드
			$this->result['fields']      = $this->Map_model->getFields($place->mapId); // 지도 커스텀 필드 로드
			$this->result['indexes']     = $this->Map_model->getIndexes($place->mapId); // 지도 마커 카테고리 로드
			$this->result['place']       = $place; // 저장되어 있는 장소 데이터 로드
			$this->result['fields_data'] = $this->Place_model->getPlaceFieldData($place->id);
			$this->result['place_flags'] = $this->Place_model->getFlags($place->id);
			$this->_render('place/edit');
		}
	}
	
	public function flags($placeId = null) {
		if( $placeId == null) { header("/"); }
		$this->_lc();
		
		$place = $this->Place_model->getPlace($placeId);
		
		if($this->form_validation->set_rules('description', 'description', 'required')->run() == true) {
			$args = $this->input->post();
			$args['placeId'] = $placeId;
			if( $this->Place_model->editPlace($args, 'modify') ) 
				redirect('/map/'.$place->mapId);	
			else 
				echo '<script type="text/javascript">alert("등록에 실패했습니다!");history.back();</script>';
		} else {
			$this->_render('place/flags');
		}
	}
	
	public function remove($placeId = null) {
		$this->load->library('form_validation');
		
		if( !$this->session->userdata('user_id') ) {
			exit('<meta charset="utf-8" /><script>alert("로그인 후 이용 가능합니다."); history.back();</script>');
		}
		
		if(!$placeId) {
			echo '<meta charset="utf-8" /><script>alert("잘못된 요청입니다."); location.href = "/";</script>';
			exit();
		}
		
		$place = $this->Place_model->getPlace($placeId);
		$this->load->library('ion_auth');
		if ($this->session->user_id == $place->userId || $this->ion_auth->is_admin()) {
			$this->db->delete('place', array('id' => $placeId));
			$this->db->delete('field_data', array('placeId' => $placeId));
			redirect("/map/".$place->mapId);
		} else {
			echo '<meta charset="utf-8" /><script>alert("삭제할 수 없습니다.");history.back();</script>';
		}
		
	}
	
	public function _lc() {
		if( !$this->session->user_id ) { 
			header("Location: /auth/login?next=".base_url($_SERVER['REQUEST_URI'])); 
			exit();
		} else {
			$this->load->helper('form');
			$this->load->library('form_validation');
		}
	}
	
	public function _render($views = null) {
		if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net") {
			$prefix = 'songnae';
		}
		$this->load->view((isset($prefix)?$prefix.'/':'').'templates/html_head', array('classes' => (isset($prefix)?$prefix.' ':'').'edit-data'));
		$this->load->view((isset($prefix)?$prefix.'/':'').'templates/gnb', array(
			'classes' => (isset($prefix)?$prefix.' ':'').'edit-data',
			'session' => $this->session->userdata(),
			'flags' => $this->Place_model->getFlags()
		));
		$this->load->view($views == null ? 'place/edit' : $views, $this->result);
	}
}

/* End of file place.php */
/* Location: ./application/controllers/place.php */
