<?php if (!defined('BASEPATH')) die();

class Users extends CI_Controller {
	public function map($userId = null) {
		$data['session'] = $this->session->userdata();
		$data['flags'] = $this->Place_model->getFlags();
		
		$c['currentNumberOfPage'] = 1; // 현재 페이지
		$c['category'] = 'all'; // 지도 카테고리
		$c['isOpen'] = 'Y';
		$c['pageSize'] = 0;
		$c['orderType'] = 'recent';
		$c['userId'] = $userId;
		$data['maps']['my'] = $this->Map_model->getMaps($c);
		$data['count']['map'] = count($data['maps']['my']['maps']);
		$data['count']['place'] = count( $this->Place_model->getPlaces(array('userId' => $userId)) ); 
		$this->load->model('User_model');
		$data['user'] = $this->User_model->getUser($userId);
		$this->load->view('templates/html_head', $data);
		$this->load->view('templates/gnb', $data);
		$this->load->view('users/map', $data);	
	}
	
	public function place($userId = null) {
		$this->load->model('Place_model');
		$this->load->model('User_model');
		$data['classes'] = 'map';
		$data['flags'] = $this->Place_model->getFlags();
		$data['session'] = $this->session->userdata();
		$data['user'] = $this->User_model->getUser($userId);
		$data['places'] = $this->Place_model->getPlaces(array('userId' => $userId));
		$c['currentNumberOfPage'] = 1; // 현재 페이지
		$c['category'] = 'all'; // 지도 카테고리
		$c['isOpen'] = 'Y';
		$c['pageSize'] = 0;
		$c['orderType'] = 'recent';
		$c['userId'] = $userId;
		$data['maps']['my'] = $this->Map_model->getMaps($c);
		$data['count']['map'] = count($data['maps']['my']['maps']);
		$data['count']['place'] = count( $data['places'] );
		$this->load->view('templates/html_head', $data);
		$this->load->view('templates/gnb', $data);
		$this->load->view('users/place', $data);
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */ 