<?php
class MY_Controller extends CI_Controller 
{
	function __construct() {
		parent::__construct();
		$this->result = array('isAdmin' => $this->ion_auth->is_admin());
    }
}
