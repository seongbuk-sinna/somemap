<?php
class MY_Model extends CI_Model 
{
    function __construct() {
    	parent::__construct();
	}
   
    public function _getPhotosUrl($filename) {
	    if(strlen($filename) < 16) $filename = 'https://img.youtube.com/vi/'.$filename.'/0.jpg';
	    else if(!(substr($filename, 0, 4) == 'http'))
			$filename = 'https://d1m8tzv6n1gicn.cloudfront.net/uploads/'.$filename;
		return $filename;
	}
	
	public function _getPhotosThumbnail($filename) {
		if(strlen($filename) < 16) $filename = 'https://img.youtube.com/vi/'.$filename.'/0.jpg';
		else if(!(substr($filename, 0, 4) == 'http'))
			$filename = 'https://d1m8tzv6n1gicn.cloudfront.net/uploads/thumbs/'.$filename;
		return $filename;
	}
}