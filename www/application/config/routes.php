<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

if($_SERVER['HTTP_HOST'] === "www.songnae.net" || $_SERVER['HTTP_HOST'] === "songnae.net") {
	$route['default_controller'] = "songnae";
} else {
	$route['default_controller'] = "main";
}

$route['404_override'] = '';
$route['map/(:num)'] = 'map/view/$1';
$route['map/(:num)/embed'] = 'map/view/$1';
$route['map/embed/(:num)'] = 'map/view/$1';
$route['map/(:num)/add-place'] = 'place/add/$1';
$route['map/(:num)/edit'] = 'map/edit/$1';
$route['map/(:num)/remove'] = 'map/remove/$1';
$route['map/(:num)/featured/(:any)'] = 'map/featured/$1/$2';
$route['place/(:num)'] = 'place/view/$1';
$route['place/(:num)/simplify'] = 'place/view/$1/simplify';
$route['place/(:num)/edit'] = 'place/edit/$1';
$route['place/(:num)/flags'] = 'place/flags/$1';
$route['place/(:num)/remove'] = 'place/remove/$1';
$route['users/(:num)/map'] = 'users/map/$1';
$route['users/(:num)/place'] = 'users/place/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */