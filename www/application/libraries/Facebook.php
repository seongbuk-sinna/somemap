<?php

class Facebook {
   
   private $appid = '';
   private $secret = '';

   public function __construct()
   {
      $ci =& get_instance();
      $ci->config->load('key');
      $this->appid = $ci->config->item('fb_appid');
      $this->secret = $ci->config->item('fb_secret');

      //load the library
      $this->load();
   }

   private function load()
   {
      require_once 'Facebook/autoload.php';
      
      $this->fb = new Facebook\Facebook([
        'app_id' => $this->appid,
        'app_secret' => $this->secret,
      ]);
   }      

}
