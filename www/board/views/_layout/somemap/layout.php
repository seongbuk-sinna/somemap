<!DOCTYPE html>
<html lang="ko">
	<head>
		<link href="/assets/typeface/NotoKR-Regular/stylesheet.css" rel="stylesheet" type="text/css">
		<title><?php echo html_escape(element('page_title', $layout)); ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
		<meta property="og:title" content="<?php echo html_escape(element('page_title', $layout)); ?>">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?=current_url()?>">
		<meta property="og:image" content="<?=isset($meta['thumbnailUrl'])&&$meta['thumbnailUrl'] !== ''?$meta['thumbnailUrl']:base_url('assets/img/front-bg.jpg')?>">
		<meta property="og:site_name" content="썸맵">
		<meta property="og:description" content="<?=isset($meta)?$meta['description']:"우리 동네 그 장소, 그 골목! 친구들과 함께 의미있는 지도를 만들어보세요."?>">
		<?php if (element('meta_description', $layout)) { ?><meta name="description" content="<?php echo html_escape(element('meta_description', $layout)); ?>"><?php } ?>
		<?php if (element('meta_keywords', $layout)) { ?><meta name="keywords" content="<?php echo html_escape(element('meta_keywords', $layout)); ?>"><?php } ?>
		<?php if (element('meta_author', $layout)) { ?><meta name="author" content="<?php echo html_escape(element('meta_author', $layout)); ?>"><?php } ?>
		<?php if (element('favicon', $layout)) { ?><link rel="shortcut icon" type="image/x-icon" href="<?php echo element('favicon', $layout); ?>" /><?php } ?>
		<?php if (element('canonical', $view)) { ?><link rel="canonical" href="<?php echo element('canonical', $view); ?>" /><?php } ?>

		<link href="/assets/style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?php echo element('layout_skin_url', $layout); ?>/css/style.css" />
		<link href="/assets/photoswipe/photoswipe.css" rel="stylesheet" type="text/css">
		<link href="/assets/photoswipe/default-skin/default-skin.css" rel="stylesheet" type="text/css">
		<link href="//s3.ap-northeast-2.amazonaws.com/somemap-live-seoul/static/js/owl.carousel.css" rel="stylesheet" type="text/css">
	</head>
	
	<body <?php echo isset($view) ? element('body_script', $view) : ''; ?>>
		<div class="l--common-gnb">
    		<div class="l--common-gnb--firstline">
		        <a href="/" class="logo">썸맵</a>
				<div class="l--filters">
					<div class="l--filters--search">
						<form method="GET" action="/explore/">
							<input type="text" name="q" value="" placeholder="" />
							<input type="hidden" name="o" value="<?=isset($_GET['o'])?$_GET['o']:'recent'?>" />
							<input type="hidden" name="c" value="<?=isset($_GET['c'])?$_GET['c']:'all'?>" />
							<input type="submit" value="Go" />
						</form>
					</div>
				</div>
				
				<?php if(isset($session['user_id'])) { ?>
	    		<?php if(count($flags) > 0) { ?>
	    		<!--label for="notification-menu" class="f--dropdown-switch notification login">
	    			<span class="s--noti-badge"><?=count($flags)?></span>
	    		</label>
		        <input type="checkbox" name="notification-menu" id="notification-menu" class="f--dropdown-checkbox" />
	    		<ul class="l--list notification">
					<?php foreach($flags as $flag) { ?>
					<li>
						<a href="/place/<?=$flag->id?>/edit">
							<div class="nm-meta"><?=$flag->mapName?></div>
							<div class="nm"><b><?=$flag->placeName?></b>의 정보 수정 제안이 등록되었습니다.</div>
							<div class="flags-date"><?=$flag->createDate?></div>
						</a>
					</li>
					<?php } ?>
		        </ul-->
		        <?php } ?>
	    		
	    		<label for="portrait-menu" class="f--dropdown-switch portrait">
		    		<span class="s--portrait" <?=isset($session['services_portrait_s']) ? 'style="background-image:url(\''.$session['services_portrait_s'].'\')"' : '' ?>></span>
		    	</label>
		        <input type="checkbox" name="portrait-menu" id="portrait-menu" class="f--dropdown-checkbox" />
	    		<ul class="l--list personal l--list--toggle">
					<li>
						<a href="/users/<?=$session['user_id']?>/map">내 지도</a>
					</li>
					<li>
						<a href="/users/<?=$session['user_id']?>/place">내 장소</a>
					</li>
					<li>
						<a href="/auth/logout?next=<?=htmlspecialchars(current_url())?>">로그아웃</a>
					</li>
		        </ul>
	    		
		        <?php } else { ?>
		        <a href="/auth/login?next=<?=current_url()?>" class="request-login">로그인</a>
		        <?php } ?>
	        </div>
	        
	        <div class="l--common-gnb--secondline">
		        <ul class="l--list l--list-hor common">
<!-- 					<li><a href="/help">도움말</a></li> -->
					<li><a href="https://www.facebook.com/somemap" target="_blank">페이스북</a></li>
					<li><a href="https://www.facebook.com/groups/somemap/" target="_blank">포럼</a></li>
					<li><a href="/board/board/notice">공지</a></li>
					<?php if(isset($session['user_id'])) { ?>
					<li>
						<a href="/map/edit">지도 만들기</a>
					</li>
					<?php } ?>
		        </ul>
	        </div>
    	</div>
    	
    	<div class="l--board">
    		<div class="l--container">
				<?php if (isset($yield))echo $yield; ?>
    		</div>
    	</div>
    	
		<div class="l--common-footer">
			<div class="l--container">
				<div class="l--common-footer--op">
					<h4><img src="/assets/img/sinna.png" height="30" alt="성북신나" /></h4>
					<h4><img src="/assets/img/logo-si.png" height="30" alt="소셜이노베이션캠프36" /></h4>
					<h4><img src="/assets/img/logo-df.png" height="30" alt="다음세대재단" /></h4>
					<p>
						<a href="https://www.facebook.com/somemap" target="_blank">페이스북 페이지</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="https://www.facebook.com/groups/somemap/" target="_blank">페이스북 썸맵 사용자 그룹</a>
					</p>
					<p>썸맵은 지역 커뮤니티에 유용한 장소들을 함께 만들어가는 지도 서비스입니다. 자발적인 참여자들이 함께 모여 36시간동안 공익적인 사회변화를 가져올 수 있는 아이디어를 실제로 구현하는 <a href="http://sicamp36.org" target="_blank">소셜이노베이션캠프 36</a>을 통해 개발되었습니다. 본 사이트는 <a href="http://daumfoundation.org" target="_blank">다음세대재단</a> <a href="http://itcanus.net/" target="_blank">아이티캐너스 기금</a> 및 카카오 후원을 통해 제작되었습니다.</p>
				</div>
			</div>
		</div>
	</body>
</html>
