"use strict";(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];
a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-65259961-1', 'auto');ga('send', 'pageview', {page: location.pathname + location.search  + location.hash});

/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *"use strict";(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];
a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-65259961-1', 'auto');ga('send', 'pageview', {page: location.pathname + location.search  + location.hash});

 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);

/*
 * somemap.js for http://somemap.kr
 * Written by San Kim
 */

var p;
var Somemap = function() {
	this.map,
	this.places = {},
	this.idxs = {},
	this.currentLocationMarker = null,
	this.scrollPos = 0,
	this.bounds = new daum.maps.LatLngBounds(),
	this.currentOverlay = null,
	this.state = 0,
	this.draftPoint = null,
	this.draftPath = null,
	this.draftType = 1,
	this.draftAddress = '',
	this.draftPathFollowing = null,
	this.draftPathCircleOverlayArray = [];
};

var dragging = null,
    ee = "mouseenter",
    le = "mouseleave";
if("ontouchstart" in window) {
	ee = "touchstart", le = "touchend";
}

Somemap.prototype.initCanvas = function(container, options) {
	this.map = new daum.maps.Map(document.getElementById(container), {
		center: options.center,
		level: options.level
	});
	
	this.map.setCopyrightPosition(daum.maps.CopyrightPosition.BOTTOMLEFT, true);
	this.map.addControl(new daum.maps.ZoomControl(), daum.maps.ControlPosition.BOTTOMLEFT);
	this.map.addControl(new daum.maps.MapTypeControl(), daum.maps.ControlPosition.BOTTOMLEFT);
	
	daum.maps.event.addListener(this.map, 'click', $.proxy(function(){ 
		if(this.currentOverlay !== null) { 
			this.currentOverlay.setMap( this.currentOverlay = null ); 
		}
	}, this));
	
	return this;
},
Somemap.prototype.addMarkersWithPlaces = function() {
	if($("#l--places").find('li.l--places--place').length > 0) {
		$("#l--places")
		.find('li.l--places--place')
		.each($.proxy(function(idx, place){
			var path = JSON.parse(place.getAttribute('data-path'));
			
			if(path.length === 1) {
				var marker = this.createMarker({
					'position': new daum.maps.LatLng(path[0].lat, path[0].lng), 
					'iconUrl' : place.getAttribute('data-iconurl') ? '/assets/img/' + place.getAttribute('data-iconurl') : '/assets/img/pin-black.png'
				});
				var center = marker.getPosition();
				var tempBounds = null;
				marker.setMap(this.map);
				this.bounds.extend(center);
			} else {
				var dPath = [], 
					tempBounds = new daum.maps.LatLngBounds(), 
					tempLatLng;
				
				for(var i = 0, length = path.length; i < length; i++) {
					tempLatLng = new daum.maps.LatLng(path[i].lat, path[i].lng);
					dPath.push(tempLatLng);
					this.bounds.extend(tempLatLng);
					tempBounds.extend(tempLatLng);
				}
			
				var marker = this.createPolyPath(dPath, path.length > 2 ? 3 : 2);
				var center = new daum.maps.LatLng(((tempBounds.getNorthEast().getLat() + tempBounds.getSouthWest().getLat()) / 2), 
												  ((tempBounds.getNorthEast().getLng() + tempBounds.getSouthWest().getLng()) / 2));
			}
			
			var overlay = new daum.maps.CustomOverlay({
				position: center, 
				content: $(place.outerHTML).addClass('overlay').clone().wrapAll('<div />').parent().html(), 
				yAnchor: -0.1, 
				zIndex: 99999999
			});
			
			this.places[place.getAttribute('data-placeid')] = {
				indexId: place.getAttribute('data-indexid'), 
				marker: marker,
				overlay: overlay,
				bounds: tempBounds
			};
			
			// 마커에 마우스 올리면 맵에 오버레이 표시
			daum.maps.event.addListener(marker, 'mouseover', $.proxy(function(){ overlay.setMap(this.map); }, this));
			// 마커에서 마우스 빠지면 오버레이 제거, 현재 보고 있는 장소의 오버레이일 경우에는 빼지 않음
			daum.maps.event.addListener(marker, 'mouseout',  $.proxy(function(){ if(this.currentOverlay !== overlay) overlay.setMap(null); }, this));
			// 마커 클릭하면 장소 정보 불러서 Append.
			daum.maps.event.addListener(marker, 'click', $.proxy(function() { 
				this.appendPlaceData(place.getAttribute('data-placeid')); 
			}, this));
		}, this));
		
		this.map.setBounds(this.bounds);
	}
	return this;
};
Somemap.prototype.createMarker = function(options) {
	var marker = new daum.maps.Marker({
	    position: options.position
	});
	
	if(options.iconUrl !== undefined) {
		marker.setImage(
			new daum.maps.MarkerImage(
				options.iconUrl, 
				new daum.maps.Size(17, 22),
				{offset: new daum.maps.Point(8, 22)}
			)
		);	
	}
	
	return marker;
};
Somemap.prototype.createPolyPath = function(path) {
	if(path.length === 2) {
		var m = new daum.maps.Polyline({
		    map: this.map,
		    path: path, // 그려질 다각형의 좌표 배열입니다
		    strokeWeight: 3, // 선의 두께입니다
		    strokeColor: '#39DE2A', // 선의 색깔입니다
		    strokeOpacity: 1, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
		    strokeStyle: 'solid', // 선의 스타일입니다
		});
	} else if(path.length > 2) {
		var m = new daum.maps.Polygon({
		    map: this.map,
		    path: path, // 그려질 다각형의 좌표 배열입니다
		    strokeWeight: 3, // 선의 두께입니다
		    strokeColor: '#39DE2A', // 선의 색깔입니다
		    strokeOpacity: 1, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
		    strokeStyle: 'solid', // 선의 스타일입니다
		    fillColor: '#A2FF99', // 채우기 색깔입니다
		    fillOpacity: 0.7 // 채우기 불투명도 입니다
		});
	}
	return m;	
};
Somemap.prototype.initCurrentLocation = function(tof) {
	if(typeof navigator.geolocation !== undefined){
		navigator.geolocation.getCurrentPosition(
			$.proxy(function(p) {
				var nowlocation = new daum.maps.LatLng(
					parseFloat( p.coords.latitude ).toFixed(2),
					parseFloat( p.coords.longitude ).toFixed(2)
				);
				
				this.map.setLevel(4);
				this.map.panTo(nowlocation);
				
				if(this.currentLocationMarker !== null) {
					this.currentLocationMarker.setMap(null);
				}
				
				if(tof) {
					this.currentLocationMarker = new daum.maps.Marker({
					    position: nowlocation,
					    image: new daum.maps.MarkerImage(
							'/assets/img/geolocation-marker.png', 
							new daum.maps.Size(38, 38),
							{offset: new daum.maps.Point(19, 19)}
						),
					    map: this.map
					});	
				} else {
					this.map.panTo(nowlocation);
					this.drawPoint({'latLng': nowlocation});
				}
			}, this),
			$.proxy(function(p) {
				this.drawPoint({'latLng': nowlocation});
			}, this),
			{enableHighAccuracy:true}
		);
	}
	return this; 
};
Somemap.prototype.initListenersForMapView = function() {
	$(".c--card-desc").html( Autolinker.link( $(".c--card-desc").html(), {
	    urls : {
	        schemeMatches : true,
	        wwwMatches    : true,
	        tldMatches    : true
	    },
	    email       : true,
	    hashtag     : 'somemap',
	    stripPrefix : true,
	    newWindow   : true,
	} ) );
	
	$("#l--index").find('.l--indexes--index-checkbox')
	.on('change', $.proxy(function(e){ // 인덱스 값 변화
		if(e.target.value === 'all') {
			if(e.target.checked) {
				$("#l--index").find('input.l--indexes--index-checkbox').prop('checked', true);
				for(var placeId in this.places) {
					this.places[placeId].marker.setMap(this.map);
					$("#l--places").find('li.l--places--place').removeClass('s--hiding');
				}
			} else {
				$("#l--index").find('input.l--indexes--index-checkbox').prop('checked', false);
				for(var placeId in this.places) {
					this.places[placeId].marker.setMap(null);
					$("#l--places").find('li.l--places--place').addClass('s--hiding'); 	
				} 
			}
		} else {
			if($(".l--indexes--index-checkbox:checked").length !== $(".l--indexes--index-checkbox").length-1) {
				$(".l--indexes--index:first-child input").prop('checked', false);
			} else {
				if($(".l--indexes--index:first-child input").prop('checked') === true) $(".l--indexes--index:first-child input").prop('checked', false);
				else $(".l--indexes--index:first-child input").prop('checked', true);
			}
			
			if(e.target.checked) {
				for(var placeId in this.places) {
					if(this.places[placeId].indexId === e.target.value) {
						this.places[placeId].marker.setMap(this.map);
						$('#l--place-' + placeId).removeClass('s--hiding');
					} 	
				}
			} else {
				for(var placeId in this.places) {
					if(this.places[placeId].indexId === e.target.value) {
						this.places[placeId].marker.setMap(null);
						$('#l--place-' + placeId).addClass('s--hiding');
					}
				}
			}
		}
	}, this));
	
	// 장소 목록에 이벤트 추가
	$('#l--places').find('.l--places--place')
	// 장소 목록 hover → 오버레이 표시
	.on(ee, $.proxy(function(e) { 
		if(ee === "touchstart") {
			dragging = false;
		} else if(ee === "mouseenter") {
			this.places[e.delegateTarget.getAttribute('data-placeid')].overlay.setMap(this.map);
		}
	}, this))
	.on('touchmove', function(){
		dragging = true;
	})
	// 장소 목록 leave → 오버레이 제거
	.on('mouseleave', $.proxy(function(e) {
		if(this.currentOverlay !== this.places[e.delegateTarget.getAttribute('data-placeid')].overlay) {
			this.places[e.delegateTarget.getAttribute('data-placeid')].overlay.setMap(null);	
		}
	}, this))
	// 장소 목록 클릭 → 페이지 로드 및 pushstate
	.on('touchend, click', $.proxy(function(e){
		if(dragging === true) return;				
		e.preventDefault();
		
		this.map.setLevel(2);
		if(this.places[e.delegateTarget.getAttribute('data-placeid')].bounds) { this.map.setBounds(this.places[e.delegateTarget.getAttribute('data-placeid')].bounds); }
		else this.map.panTo( this.places[e.delegateTarget.getAttribute('data-placeid')].marker.getPosition() );
		this.appendPlaceData( e.delegateTarget.getAttribute('data-placeid') );
	}, this));
	
	document.getElementById('map-current-location').addEventListener('click', $.proxy(function(){
		this.initCurrentLocation(true);
	}, this));
	document.getElementById('toggle-switch-map-list').addEventListener('change', $.proxy(function(e){
		if(e.target.checked) this.scrollPos = $(document).scrollTop();	
		setTimeout($.proxy(function(){
			$(document).scrollTop(this.scrollPos);
			this.map.relayout();
		}, this), 200);
	}, this));
	$('body').on('click', '#f--close-latest-place', $.proxy(function(){
		history.back();
	}, this));

	new List("l--map--wrapper", { valueNames: ['l--places--place-name', 'l--places--place-id','l--places--place-index'] });

	return this;
};
Somemap.prototype.stateChange = function(state) {
	document.getElementById('c--map-controller--draw-state-' + (state === 3 ? 2 : state)).checked = true;
	p = this;
	
	switch(this.state = state) {
		case 0:
			daum.maps.event.removeListener(this.map, 'click', this.drawPoint);
			daum.maps.event.removeListener(this.map, 'click', this.drawPath);
			break;
		case 1:
			if(this.draftPoint === null) this.draftPoint = this.createMarker({'position': new daum.maps.LatLng(0, 0)});
			if(this.draftPoint !== null && this.draftPoint.getPanels() === null) this.draftPoint.setMap(this.map);
			if(this.draftPath !== null && this.draftPath.getPanels() !== null) this.draftPath.setMap(null);
			if(this.draftPathFollowing !== null && this.draftPathFollowing.getPanels() !== null) {
				this.draftPathFollowing.setMap(null);
				for(var i = 0, length = this.draftPathCircleOverlayArray.length; i < length; i++) {
					this.draftPathCircleOverlayArray[i].setMap(null);
				}
				this.draftPathCircleOverlayArray = [];
				this.draftPath = null;
			}
			if(this.draftPathCircleOverlayArray.length > 0) {
				for(var i = 0, length = this.draftPathCircleOverlayArray.length; i < length; i++) {
					this.draftPathCircleOverlayArray[i].setMap(null);
				}
			}
			daum.maps.event.removeListener(this.map, 'click', this.drawPath);
			daum.maps.event.addListener(this.map, 'click', this.drawPoint);
			break;
		case 2:
		case 3:
			if(this.draftPath === null) document.getElementById('visible-required-item').disabled = true;
			if(this.draftPoint !== null && this.draftPoint.getPanels() !== null) this.draftPoint.setMap(null);
			if(this.draftPath !== null && this.draftPath.getPanels() === null) this.draftPath.setMap(this.map);
			if(this.draftPathFollowing === null) {
				this.draftPathFollowing = new daum.maps.Polyline({
		            strokeWeight: 3,
		            strokeColor: '#db4040',
		            strokeOpacity: 0.5,
		            strokeStyle: 'solid'    
		        });
			}
			if(this.draftPathCircleOverlayArray.length > 0) {
				for(var i = 0, length = this.draftPathCircleOverlayArray.length; i < length; i++) {
					this.draftPathCircleOverlayArray[i].setMap(this.map);
				} 
			}
			daum.maps.event.removeListener(this.map, 'click', this.drawPoint);
			daum.maps.event.addListener(this.map, 'click', this.drawPath);
			break;
	}
	return this;
};
Somemap.prototype.drawPoint = function(e) {
	if(p.draftPoint.getPanels() === null) p.draftPoint.setMap(p.map);
	p.draftPoint.setPosition(e.latLng);
	p.draftType = 1;
	new daum.maps.services.Geocoder().coord2Address(e.latLng.ib, e.latLng.jb, function(result, status){
		if(status === daum.maps.services.Status.OK) {
			p.draftAddress = result[0].road_address ? result[0].road_address.address_name : result[0].address.address_name;
		} 
	});
	document.getElementById('visible-required-item').disabled = false;
};
Somemap.prototype.drawPath = function(e) {
	if(p.draftPathFollowing.getPanels() === null) {
		if(p.draftPath !== null) p.draftPath.setMap(null);
		p.draftPath = new daum.maps.Polyline({
            map: this.map,
            strokeWeight: 3, 
            strokeColor: '#db4040',
            strokeOpacity: 1,
            strokeStyle: 'solid'
        });
        p.draftPath.setMap(p.map);
        p.draftPathFollowing.setMap(p.map);
        daum.maps.event.addListener(p.map, 'mousemove', p.drawFollowingPath);
        document.getElementById('visible-required-item').disabled = true;
	}
	
	var path = p.draftPath.getPath();
	path.push(e.latLng);
	p.draftPath.setPath(path);
	
	var circle = new daum.maps.CustomOverlay({
        content: '<span class="path-dot" data-timestamp="'+ Date.now() +'"></span>',
        position: e.latLng,
        zIndex: 1,
        clickable: true
    });
	
    circle.setMap(p.map);
    p.draftPathCircleOverlayArray.push(circle);
};
Somemap.prototype.drawFollowingPath = function(e) {
	if(p.draftPath !== null && p.draftPath.getPath().length > 0) {
		var currentPath = p.draftPath.getPath();
		p.draftPathFollowing.setPath([currentPath[currentPath.length - 1], e.latLng]);	
	}
};
Somemap.prototype.processDots = function(e) {
	// 그리고 있는 선의 경로
	var path = e.data.this.draftPath.getPath(),
	// 클릭한 원의 타임스탬프
	tt = e.target.getAttribute('data-timestamp'),
	// 첫번째 원의 타임스탬프
	oft = $('.path-dot').first().attr('data-timestamp'),
	// 마지막 원의 타임스탬프
	olt = $('.path-dot').last().attr('data-timestamp');
	
	// 클릭한 원이 첫번째 원일때 -> 도형을 그린다
	if(tt === oft || tt === olt) {
		if(tt === oft) e.data.this.draftType = 3;
		else if(tt === olt) e.data.this.draftType = 2;
		
		// 그리고 있는 선 맵에서 뺀다
		e.data.this.draftPath.setMap(null);
		// 마우스 커서 따라다니는 임시 선 맵에서 뺀다
		e.data.this.draftPathFollowing.setMap(null);
		e.data.this.draftPathFollowing.setPath([]);
		
		// 그리고 있는 선 경로 정보 가져옴
		var path = e.data.this.draftPath.getPath();
		// 가져온 정보를 바탕으로 도형 그릴 준비하고
		e.data.this.draftPath = e.data.this.createPolyPath(path, e.data.this.draftType);
		// 맵에 올린다
		e.data.this.draftPath.setMap(e.data.this.map);
		
		// 원형 클릭 점들 맵에서 뺀다
		for(var i = 0, length = e.data.this.draftPathCircleOverlayArray.length, jsonpath = []; i < length; i++) {
			e.data.this.draftPathCircleOverlayArray[i].setMap(null);
			jsonpath.push({'lat': path[i].getLat(), 'lng': path[i].getLng()});
		} 
		// 원형 점 저장하고 있는 배열 초기화
		e.data.this.draftPathCircleOverlayArray = [];
		// 체크박스 비활성화 해제
		document.getElementById('visible-required-item').disabled = false;
		daum.maps.event.removeListener(e.data.this.map, 'mousemove', e.data.this.drawFollowingPath);
	// 클릭한 원이 마지막 -> 선을 그린다 
	} else {
		// 인덱스 구해서
		var idx = $('.path-dot').index($('.path-dot[data-timestamp='+e.target.getAttribute('data-timestamp')+']'));
		// 클릭 포인트 빼고
		e.data.this.draftPathCircleOverlayArray[idx].setMap(null);
		// 배열에서 빼고
		e.data.this.draftPathCircleOverlayArray.splice(idx, 1);
		// 그리고 있는 선 배열에서도 빼고
		path.splice(idx, 1);
		// 선 다시 그린다
		e.data.this.draftPath.setPath(path);
	}
};
Somemap.prototype.initKeywordSearch = function(){
	var places = new daum.maps.services.Places();
	var geocoder = new daum.maps.services.Geocoder();
	var searchData = '';
	var template = document.getElementById('template--search-result').innerHTML;
	Mustache.parse(template);
	var defaultlist = Mustache.render(template, {
		title:    "찾는 장소가 없나요? 직접 추가해보세요.",
		dataLat:  37.566677,
		dataLng:  126.978414,
		dataPath: JSON.stringify([{'lat': 37.566677, 'lng': 126.978414}]),
		dataType: 1
	});
	
	//　키워드 서치 후 템플릿 렌더링
	document.getElementById('title').addEventListener('keyup', $.proxy(function(e){
		if(e.target.value.trim() !== "") {
			document.getElementById('l--edit').appendChild(document.getElementById('c--map'));
			searchData = defaultlist;
			
			places.keywordSearch(e.target.value, function(result, status){
				if(status === daum.maps.services.Status.OK) {
					for(var i = 0, length = result.length; i < length; i++) {
						searchData += Mustache.render(template, {
							dataTitle:   result[i].place_name,
							title:       result[i].place_name,
							category:    result[i].category_name,
							dataPath:    JSON.stringify([{'lat': result[i].y, 'lng': result[i].x}]),
							dataAddress: result[i].road_address_name !== "" ? result[i].road_address_name : result[i].address_name, 
							address:     result[i].road_address_name !== "" ? result[i].road_address_name : result[i].address_name
						});
					}
					document.getElementById('l--places-search').innerHTML = searchData;
				} else if(status === daum.maps.services.Status.ZERO_RESULT) {
					document.getElementById('l--places-search').innerHTML = defaultlist;
					/*
					geocoder.addr2coord(e.target.value, function(status, result){
						if(status === daum.maps.services.Status.OK) {
							console.log(status, result);
							for(var i = 0, length = result.length; i < length; i++) {
								searchData += Mustache.render(template, {
									title:       result[i].road_address_name !== "" ? result[i].road_address_name : result[i].place_name,
									dataAddress: result[i].road_address_name !== "" ? result[i].road_address_name : result[i].place_name, 
									address:     result[i].road_address_name !== "" ? result[i].road_address_name : result[i].place_name,
									dataPath:    JSON.stringify([{'lat': result[i].y, 'lng': result[i].x}])
								});
							}
							document.getElementById('l--places-search').innerHTML = searchData;
						}
					});
					*/
				}
			});
		}
	}, this));
	
	// 장소 검색 결과 클릭 시 이벤트 처리
	$("#l--places-search").on('click', 'a.l--places--place--wrapper', $.proxy(function(e){
		e.preventDefault();
		// 데이터 캐싱
		var thisnode = e.currentTarget.parentNode;
		var path = JSON.parse(thisnode.getAttribute('data-path'));
		
		// 맵 DOM 이동, 사이즈 조정
		thisnode.appendChild(document.getElementById('c--map'));
		this.map.relayout();

		// 지도 이동시 중앙 점 마커 등록
		var _this = this;
		daum.maps.event.addListener(this.map, 'dragend', function() {
			var latlng = _this.map.getCenter();
			document.getElementById('path').value = JSON.stringify([{'lat': latlng.getLat(), 'lng': latlng.getLng()}]);
			_this.draftPoint.setPosition(latlng);
		});

		// 마커 그리기
		if(path.length === 1) {
			var position = new daum.maps.LatLng(path[0].lat, path[0].lng);
			if(this.draftPoint === null) this.draftPoint = this.createMarker({'position': position});
			else this.draftPoint.setPosition(position);
			this.draftPoint.setMap(this.map);
			this.map.setLevel(3);
			this.map.panTo(position);
			this.draftAddress = thisnode.getAttribute('data-address');
			this.stateChange(1).stateChange(0);
			document.getElementById('visible-required-item').disabled = false;
		} else if(path.length > 1) {
			var bounds = new daum.maps.LatLngBounds();
			for(var i = 0, dPath = [], length = path.length; i < length; i++) {
				var latLng = new daum.maps.LatLng(path[i].lat, path[i].lng);
				dPath.push(latLng);
				bounds.extend(latLng);
			}
			if(this.draftPath === null) this.draftPath = this.createPolyPath(dPath, path.length > 2 ? 3 : 2);
			else this.draftPath.setPath(dPath);
			this.draftPath.setMap(this.map);
			this.map.setBounds(bounds);	
		} 
	}, this));
	
	document.getElementById('visible-required-item').addEventListener('change', $.proxy(function(e){
		var data = e.target.parentNode.parentNode;
		if(this.draftPoint !== null && this.draftPoint.getPanels() !== null) {
			document.getElementById('address').value   = this.draftAddress;
			document.getElementById('path').value = JSON.stringify([{'lat': this.draftPoint.getPosition().getLat(), 'lng': this.draftPoint.getPosition().getLng()}]);
		} else if(this.draftPath !== null && this.draftPath.getPanels() !== null) {
			document.getElementById('address').value   = '';
			for(var i = 0, path = this.draftPath.getPath(), jsonpath = [], length = path.length; i < length; i++) {
				jsonpath.push({'lat': path[i].getLat(), 'lng': path[i].getLng()});
			} 
			document.getElementById('path').value = JSON.stringify(jsonpath);
		}
	}, this));
	
	return this;	
};
Somemap.prototype.initListenersForDataEdit = function(){
	document.getElementById('map-current-location').addEventListener('click', $.proxy(function(){
		this.stateChange(1);
		this.initCurrentLocation(false);
	}, this));
	$(".l--edit--uploads-list").sortable();
};
Somemap.prototype.loadPlaceData = function(placeId) {
	$(".c--card-data").html( Autolinker.link( $(".c--card-data").html(), {
	    urls : {
	        schemeMatches : true,
	        wwwMatches    : true,
	        tldMatches    : true
	    },
	    email       : true,
	    stripPrefix : true,
	    newWindow   : true,
	} ) );
	
	var a = $('.c--card-data[data-placeid='+placeId+']');
	var path = a.data('path');
	
	this.initCanvas('place-canvas-'+placeId, {
		'center': new daum.maps.LatLng(path[0].lat, path[0].lng),
		'level' : 3
	});
	
	if(path.length === 1) {
		var marker = this.createMarker({'position': new daum.maps.LatLng(path[0].lat, path[0].lng)});
		marker.setMap(this.map);	
		this.map.setCenter(marker.getPosition());
	} else {
		var path = a.data('path'),
			dPath = [], 
			tempBounds = new daum.maps.LatLngBounds(), 
			tempLatLng;

		for(var i = 0, length = path.length; i < length; i++) {
			tempLatLng = new daum.maps.LatLng(path[i].lat, path[i].lng);
			dPath.push(tempLatLng);
			tempBounds.extend(tempLatLng);
		}
		this.createPolyPath(dPath, path.length > 2 ? 3 : 2);
		this.map.setBounds(tempBounds);
	}
	  
	$('.owl-carousel').owlCarousel({
	    loop: true,
	    items: 1,
	    nav: true,
	    mouseDrag: false,
	    autoplay: !("ontouchstart" in window),
	    autoplayTimeout: 5000
	});
	
	var
	// ACTIVITY INDICATOR

	activityIndicatorOn = function()
	{
		$( '<div id="imagelightbox-loading"><div></div></div>' ).appendTo( 'body' );
	},
	activityIndicatorOff = function()
	{
		$( '#imagelightbox-loading' ).remove();
	},


	// OVERLAY

	overlayOn = function()
	{
		$( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
		$( '.l--common-gnb' ).css('z-index', 'initial');
	},
	overlayOff = function()
	{
		$( '#imagelightbox-overlay' ).remove();
		$( '.l--common-gnb' ).css('z-index', '10001');
	},


	// CLOSE BUTTON

	closeButtonOn = function( instance )
	{
		$( '<button type="button" id="imagelightbox-close" title="Close"></button>' ).appendTo( 'body' ).on( 'click touchend', function(){ $( this ).remove(); instance.quitImageLightbox(); return false; });
	},
	closeButtonOff = function()
	{
		$( '#imagelightbox-close' ).remove();
	},


	// CAPTION

	captionOn = function()
	{
		var description = $( 'a[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"] img' ).attr( 'alt' );
		if( description.length > 0 )
			$( '<div id="imagelightbox-caption">' + description + '</div>' ).appendTo( 'body' );
	},
	captionOff = function()
	{
		$( '#imagelightbox-caption' ).remove();
	},


	// NAVIGATION

	navigationOn = function( instance, selector )
	{
		var images = $( selector );
		if( images.length )
		{
			var nav = $( '<div id="imagelightbox-nav"></div>' );
			for( var i = 0; i < images.length; i++ )
				nav.append( '<button type="button"></button>' );

			nav.appendTo( 'body' );
			nav.on( 'click touchend', function(){ return false; });

			var navItems = nav.find( 'button' );
			navItems.on( 'click touchend', function()
			{
				var $this = $( this );
				if( images.eq( $this.index() ).attr( 'href' ) != $( '#imagelightbox' ).attr( 'src' ) )
					instance.switchImageLightbox( $this.index() );

				navItems.removeClass( 'active' );
				navItems.eq( $this.index() ).addClass( 'active' );

				return false;
			})
			.on( 'touchend', function(){ return false; });
		}
	},
	navigationUpdate = function( selector )
	{
		var items = $( '#imagelightbox-nav button' );
		items.removeClass( 'active' );
		items.eq( $( selector ).filter( '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ).index( selector ) ).addClass( 'active' );
	},
	navigationOff = function()
	{
		$( '#imagelightbox-nav' ).remove();
	},


	// ARROWS

	arrowsOn = function( instance, selector )
	{
		var $arrows = $( '<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>' );

		$arrows.appendTo( 'body' );

		$arrows.on( 'click touchend', function( e )
		{
			e.preventDefault();

			var $this	= $( this ),
				$target	= $( selector + '[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' ),
				index	= $target.index( selector );

			if( $this.hasClass( 'imagelightbox-arrow-left' ) )
			{
				index = index - 1;
				if( !$( selector ).eq( index ).length )
					index = $( selector ).length;
			}
			else
			{
				index = index + 1;
				if( !$( selector ).eq( index ).length )
					index = 0;
			}

			instance.switchImageLightbox( index );
			return false;
		});
	},
	arrowsOff = function()
	{
		$( '.imagelightbox-arrow' ).remove();
	};
	
	var selectorF = '.owl-item:not(.cloned) .item a';
	var instanceF = $( selectorF ).imageLightbox({
		animationSpeed: 100,
		preloadNext: true,
		onStart:		function() { overlayOn(); closeButtonOn( instanceF ); arrowsOn( instanceF, selectorF ); },
		onEnd:			function() { overlayOff(); captionOff(); closeButtonOff(); arrowsOff(); activityIndicatorOff(); },
		onLoadStart: 	function() { activityIndicatorOn(); },
		onLoadEnd:	 	function() { activityIndicatorOff(); $( '.imagelightbox-arrow' ).css( 'display', 'block' ); }
	});

	
	
	$("#f--remove-place-trigger").on('click', function(e){
		if(confirm("이 장소를 정말 삭제하시겠어요?") === true) {
			location.href = '/place/' + e.currentTarget.getAttribute('data-placeid') + '/remove';
		} e.preventDefault();
	});
};
Somemap.prototype.appendPlaceData = function(placeId) {
	
	history.pushState({placeId: placeId}, "PLACE", '/place/'+placeId);
	
	if(this.currentOverlay !== null) this.currentOverlay.setMap( this.currentOverlay = null );
// 	$(".l--map--place-wrapper").remove();
	
	this.currentOverlay = this.places[placeId].overlay;
	this.currentOverlay.setMap(this.map);
	
	$(".l--common-gnb").after(
		$('<div class="l--map--place-wrapper ios-scroll"></div>')
		.load('/place/' + placeId + '/simplify', $.proxy(function(){
			var pv = new Somemap();
			pv.loadPlaceData(placeId);
			this.scrollPos = $(document).scrollTop();
			$(document).scrollTop(0), $('body').addClass('noscroll');
		}, this)).focus()
	);
};

if( !($(".l--edit--indexes .l--edit--block--wrapper-relative").length>0) ) $("#f--add-index").click();

$('body').on('click', '#f--add-field', function(){ 
	// 커스텀 필드 추가
	$('.l--edit--fields').append(document.getElementById('template-new-field').innerHTML);
}).on('click', '#f--add-index', function(){ 
	// 범례 추가
	var template = $('#template-new-index').html();
	Mustache.parse( template );   // optional, speeds up future uses
    $('.l--edit--indexes').append( Mustache.render(template, {timestamp: Date.now()}) );
}).on("click", ".f--remove-field, .f--remove-index", function(e) {
	var exterminate = $(this).parent();
	if(exterminate.hasClass("unregistered")) {
		exterminate.remove();
	} else if (confirm("정말로 삭제하시겠어요? 삭제하는 경우, 다른 장소에 등록된 내용들도 함께 삭제됩니다.")) {
		$.ajax({
			type:"POST",
			url:'/api/deleteFields',
			data: {something_id:exterminate.attr("id")},
			dataType: "json",
			success : function(res) {
				if( res.result === true ) exterminate.remove();
				else alert('삭제하지 못했습니다.');
			},
			complete : function(res) {
			},
			error : function(xhr, status, error) {
				alert("에러발생");
			}
		});
	} e.preventDefault();
}).on('click', '.f--change-icon, .f--change-icon .l--tooltip button', function(e){
	e.preventDefault();
}).on('click', '.f--change-icon .l--tooltip button', function(){
	$("img[data-target="+$(this).data('target')+"]").attr('src', '/assets/img/'+$(this).data('source'));
	$("[name=\'index_image[]\'][data-target="+$(this).data('target')+"]").val($(this).data('source'));
}).on("click", ".l--edit--uploads-photo .overlay", function(){
	// 사진 삭제
	$(this).parent().remove();
});
$('#fileupload').fileupload({
    url: '/api/uploadPhoto',
    dataType: 'json',
    autoUpload: true,
    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
    maxFileSize: 999000,
    disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true
}).on('fileuploadadd', function (e, data) {
	console.log(data['files']);
	$('<div/>').addClass('l--edit--uploads-photo').attr('data-filename', data['files'][0].name + data['files'][0].size).appendTo('.l--edit--uploads-list');
	$(".l--edit--uploads-list").sortable();
}).on('fileuploaddone', function (e, data) {
	$("[data-filename='"+data['files'][0].name+data['files'][0].size+"']")
		.append($('<div />').addClass('photo').css('background', 'url(\''+data.result.body.url+'\') no-repeat center / cover'))
		.append('<input name="photos_filename[]" type="hidden" value="'+data.result.body.filename+'" />')
		.append('<div class="overlay">삭제?</div>');
});
