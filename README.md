# 썸맵

## 환경
* PHP 5.3.10+
* MySQL 5.5.38+
* Apache 2.2.22+

## 개발 서버
* VirtualBox, Vagrant, Ansible 설치
* vagrant up
* then connect to 192.168.33.19 that we already set

## 설정해야 할 것
* config/key.php
* config/development/database.php, config/production/database.php
* S3 사용시: core/MY_Model.php

## 사용중인 라이브러리
* PHPExcel
* AWS SDK for PHP v3.12.1
* Ion Auth v2.5.2
* jQuery v1.12.0
* jQuery UI v1.11.4
* jQuery File Upload Plugin
* jQuery Iframe Transport Plugin
* jQuery Linky v0.1.8
* OwlCarousel